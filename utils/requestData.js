var _Caiyuanzi = require('data.js');
module.exports = {
    cyz_requestGet: function (url, data, suc, fail) {
        data.utoken = wx.getStorageSync("wuliu_utoken")||'';
        data.app_token = _Caiyuanzi.caiyuanzi_app_token;
        data.daili_id = _Caiyuanzi.caiyuanzi_daili_id;
        data.daili_type = _Caiyuanzi.caiyuanzi_is_daili;
        wx.request({
            url: _Caiyuanzi.caiyuanzi_host_api_url + url,
            data: data,
            header: {
                'content-type': 'application/json',
            },
            method: 'GET',
            success: function (res) {
                if(res.data.code == 33){
                    wx.hideToast();
                    wx.reLaunch({url: '/pages/user_disabled/index'});
                    return false;
                }
                if (res.data.code == 44 || res.data.code == 55) {
                    wx.hideToast();
                    wx.showModal({
                        title: '提示',
                        content: res.data.info,
                        showCancel: false,
                        success: function () {
                            wx.reLaunch({ url: '/pages/index/index' });
                        }
                    });
                    return false;
                }
                if (suc) {
                    suc(res.data);
                }
            },
            fail: function (res) {
                /*
                wx.showModal({
                    title: '提示',
                    content: 'error:网络请求失败',
                    showCancel: false
                })*/
                return false;
            }
        });
    },
    cyz_requestPost: function (url, data, suc, fail) {
        data.utoken = wx.getStorageSync("wuliu_utoken");
        data.app_token = _Caiyuanzi.caiyuanzi_app_token;
        data.daili_id = _Caiyuanzi.caiyuanzi_daili_id;
        data.daili_type = _Caiyuanzi.caiyuanzi_is_daili;
        wx.request({
            url: _Caiyuanzi.caiyuanzi_host_api_url + url,
            data: data,
            method: 'POST',
            header: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'client': 'XCX',
            },
            success: function (res) {
                if (suc) {
                    suc(res.data);
                }
            },
            fail: function (res) {
                wx.showModal({
                    title: '提示',
                    content: 'error:网络请求失败',
                    showCancel: false
                })
                return false;
            }
        });
    },
    cyz_uploadPic: function (url, imgurl,data, suc, fail) {
        data.utoken = wx.getStorageSync("wuliu_utoken");
        data.app_token = _Caiyuanzi.caiyuanzi_app_token;
        data.daili_id = _Caiyuanzi.caiyuanzi_daili_id;
        data.daili_type = _Caiyuanzi.caiyuanzi_is_daili;
        wx.uploadFile({
            url: _Caiyuanzi.caiyuanzi_host_api_url + url,
            filePath: imgurl,
            name: 'file',
            formData: data,
            success: function (res) {
                if (suc) {
                    suc(res.data);
                }
            },
            fail: function (res) {
                wx.showModal({
                    title: '提示',
                    content: '图片上传失败',
                    showCancel: false
                })
                return false;
            }
        })
    }
}