const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_c_config: [],
        submitIsLoading: false,
        buttonIsDisabled: false,
        this_jinbi_pay_open: 0,
        is_alert_content: true
    },
    set_alert_content_close: function () {
        this.setData({ is_alert_content: false });
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/BusinessVipApi/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 1) {
                that.setData({  
                    this_jinbi_pay_open: xyz_data.info.jinbi_is_open, 
                    this_c_config: xyz_data.info 
                });
                if (xyz_data.info.shop_level == -2 || xyz_data.info.shop_level == -1){
                    wx.showToast({
                        title: xyz_data.info.shop_level_msg,
                        icon: 'none',
                        duration: 3000
                    })
                }
                WxParse.wxParse('article', 'html', xyz_data.info.shop_shengji_shuoming, that, 5);
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            }else{
                _functionCYZ.CYZ_alert(xyz_data.info, '/pages/user/index', 'tab');
                return false;
            }

        });
    },
    //进行升级支付
    go_pay_bind: function () {
        var that = this;
        that.go_recharge_bind();
    },
    //确认充值
    go_recharge_bind: function () {
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/BusinessVipApi/pay', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_recharge_bind();
                });
                return false;
            } else if (xyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': xyz_data.info.timeStamp,
                    'nonceStr': xyz_data.info.nonceStr,
                    'package': xyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': xyz_data.info.paySign,
                    'success': function (res) {
                        _functionCYZ.CYZ_alert('升级成功', '/pages/user/index', 'tab');
                    },
                    'fail': function (res) {

                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    go_jinbi_pay_bind: function () {
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var requestData = {};
        requestData.sj_type = that.data.this_sj_type;
        _requsetCYZ.cyz_requestGet('/ShopShengji/accountPay', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_jinbi_pay_bind();
                });
                return false;
            } else if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert('升级成功', '/pages/user/index', 'tab');
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //下拉刷新
    onPullDownRefresh: function () {
        var that = this;
        that.onShow();
        setTimeout(() => {
            wx.stopPullDownRefresh();
        }, 1000);
    },
});