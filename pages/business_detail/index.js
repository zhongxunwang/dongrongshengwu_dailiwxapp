const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
const QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');// 引入SDK核心类
const qqmapsdk = new QQMapWX({ key: 'WFABZ-MNEW3-JWC3B-YNHD3-XKVM3-4OBY2' });// 实例化API核心类
Page({
    data: {
        this_config_data: null,
        this_app_sys:null,
        this_shop_id:0,
        this_c_config:null,
        this_shop_data:null,
        this_quan_data:null,
        this_shop_info_data:null,
        this_u_lat: 0,
        this_u_lng: 0,
        this_login_user_id: 0,
        this_config_op: null,
        this_fuwu_info:null,
        is_show_alert:false
    },
    getLocation: function () {
        var that = this;
        _functionCYZ.CYZ_loading("定位中，请稍候...");
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                that.setData({ this_u_lat: res.latitude, this_u_lng: res.longitude });
                that.getIndexData();
            },
            fail: function () {
                //弹出系统设置
                wx.openSetting({
                    success: (res) => {
                        if (res.authSetting['scope.userLocation'] == false) {
                            wx.showModal({
                                title: '提示',
                                content: "请允许地理位置授权",
                                showCancel: false,
                                success: function () {
                                    that.getLocation();
                                }
                            });
                        } else {
                            that.getLocation();
                        }
                    }
                });
                return false;
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    },
    onLoad: function (op) {
        this.setData({ this_shop_id: op.id,this_config_op: op});
        this.getLocation();
    },
    getIndexData: function () {
        var that = this;
        let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
        });

        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.shop_id = that.data.this_shop_id;
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/BusinessApi/getShopInfo', requestData, function (cyz_data) {
            console.log(cyz_data)
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_config_data: cyz_data.info.index_config,
                    this_shop_data: cyz_data.info.shop_data_info,
                    this_quan_data: cyz_data.info.shop_quan_list,
                    this_c_config: cyz_data.info.shop_c_config,
                    this_shop_info_data: cyz_data.info.shop_info_list,
                    this_app_sys: cyz_data.info.daili_copy_info
                });
                WxParse.wxParse('article', 'html', cyz_data.info.shop_data_info.shop_jianjie, that, 5);
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info, "../business/index", "tab");
                return false;
            }
            
        });
    },
    /**
	 * 拨打电话
	 */
    onCallTap: function (e) {
        const mobile = e.currentTarget.dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    //广告链接跳转
    go_ad_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    //返回首页
    go_home_bind: function (e) {
        wx.switchTab({ url: "../business/index"});
    },
    //地图跳转
    go_map_info_b: function () {
        var that = this;
        wx.openLocation({
            latitude: parseFloat(that.data.this_shop_data.shop_lat),
            longitude: parseFloat(that.data.this_shop_data.shop_lng),
            scale: 18,
            address: that.data.this_shop_data.shop_address
        });
    },
    //复制微信
    get_weixin_bind:function(){
        var that = this;
        wx.setClipboardData({
            data: that.data.this_shop_data.shop_wenxin||'',
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                    icon: 'success',
                    duration: 2000
                })
            }
        })
    },
    //图片预览
    preview_look_img:function(e){
        wx.previewImage({
            current: '',
            urls: [e.currentTarget.dataset.url]
        })
    },
    //收藏
    shop_shoucang_bind:function(){
        var that = this;
        _functionCYZ.CYZ_loading_n("操作中");
        var requestData = {};
        requestData.shop_id = that.data.this_shop_id;
        requestData.action_type = 'shoucang';
        _requsetCYZ.cyz_requestGet('/BusinessApi/shopShoucang', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.getIndexData();
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.shop_shoucang_bind();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //获取二维码
    get_wxcode_img_bind:function(){
        var that = this;
        _functionCYZ.CYZ_loading_n("生成中");
        var requestData = {};
        requestData.shop_id = that.data.this_shop_id;
        _requsetCYZ.cyz_requestGet('/BusinessApi/getBusinessWxCodeImg', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                wx.previewImage({
                    current: '', // 当前显示图片的http链接
                    urls: [xyz_data.info] // 需要预览的图片http链接列表
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //领取优惠券
    quan_lingqu_bind: function (e) {
        var that = this;
        let this_quan_data = that.data.this_quan_data;
        let quan_id = e.currentTarget.dataset.id || 0;
        let t_index = e.currentTarget.dataset.index || 0;
        _requsetCYZ.cyz_requestGet('/ApiQuanUser/userLingQuan', { quan_id: quan_id }, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                this_quan_data[t_index].user_is_ling = 1;
                that.setData({ this_quan_data: this_quan_data });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.quan_lingqu_bind(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //服务详情
    show_fuwu_info_bind: function (e) {
        var that = this;
        let fuwu_id = e.currentTarget.dataset.id;
        let shop_id = that.data.this_shop_id;
        // _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.fuwu_id = fuwu_id;
        requestData.shop_id = shop_id;
        _requsetCYZ.cyz_requestGet('/DanyeApi/getFuwuInfo', requestData, function (cyz_data) {
            // wx.hideToast();
            that.setData({
                this_fuwu_info: cyz_data.info
            });
            if (cyz_data.info.is_show_alert == 1) {
                WxParse.wxParse('article_fuwu', 'html', cyz_data.info.f_content, that, 5);
                that.setData({ is_show_alert: true });
            } else {
                that.fuwu_call_bind();
            }
        });
    },
    fuwu_call_bind: function () {
        var that = this;
        if (that.data.this_fuwu_info.f_phone == '') {
            return false;
        }
        wx.showModal({
            title: '提示',
            content: '服务电话：' + that.data.this_fuwu_info.f_phone,
            showCancel: true,
            cancelText: '取消',
            confirmText: '拨打',
            success: function (res) {
                if (res.confirm) {
                    wx.makePhoneCall({ phoneNumber: that.data.this_fuwu_info.f_phone });
                } else if (res.cancel) {

                }
            }
        })
    },
    close_alert_bind: function () {
        this.setData({ is_show_alert: false });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_shop_data.shop_name;
        var sharePath = 'pages/business_detail/index?id=' + that.data.this_shop_id + '&tg_user_id=' + that.data.this_login_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})