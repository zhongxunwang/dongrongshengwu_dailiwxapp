const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        submitIsLoading: false,
        buttonIsDisabled: false,
        this_rc_config:null,
        this_rc_ohers_active:false,
        this_rc_jiner:0,
        this_input_jiner:0
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/Recharge/getRzConfig', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ this_rc_config: xyz_data.info});
        });
    },
    //选择充值金额
    select_rc_bind:function(e){
        var that = this;
        let rc_config = that.data.this_rc_config;
        for(var i=0;i<rc_config.length;i++){
            rc_config[i].is_select = false;
        }
        rc_config[e.currentTarget.id].is_select = true;
        that.setData({ this_rc_config: rc_config, this_rc_ohers_active: false, this_rc_jiner: rc_config[e.currentTarget.id].rc_man, this_input_jiner: 0});
    },
    //充值其它金额
    select_rc_others_bind:function(){
        var that = this;
        let rc_config = that.data.this_rc_config;
        for (var i = 0; i < rc_config.length; i++) {
            rc_config[i].is_select = false;
        }
        that.setData({ this_rc_config: rc_config, this_rc_ohers_active: true, this_rc_jiner: 0});
    },
    get_rc_jiner:function(e){
        var that = this;
        let rz_jiner = parseFloat(e.detail.value);
        var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
        if (reg.test(rz_jiner)) {
            if (rz_jiner < 1 || rz_jiner>10000){
                wx.showModal({
                    title: '提示',
                    content: "单笔充值金额1-10000元",
                    showCancel: false
                });
                that.setData({ this_rc_jiner: 0, this_input_jiner:0});
                return false;
            }
            that.setData({ this_rc_jiner: rz_jiner, this_input_jiner: rz_jiner});
        } else {
            wx.showModal({
                title: '提示',
                content: "充值金额格式错误",
                showCancel: false
            });
            that.setData({ this_rc_jiner: 0, this_input_jiner: 0 });
            return false;
        };
    },
    //确认充值
    go_recharge_bind:function(){
        var that = this;
        let cz_jiner = parseFloat(that.data.this_rc_jiner);
        if (cz_jiner <= 0){
            wx.showModal({
                title: '提示',
                content: "请选择或输入购买数量",
                showCancel: false
            });
            return false;
        }
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var requestData = {};
        requestData.rz_account = cz_jiner;
        _requsetCYZ.cyz_requestGet('/Recharge/index', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_recharge_bind();
                });
                return false;
            } else if (xyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': xyz_data.info.timeStamp,
                    'nonceStr': xyz_data.info.nonceStr,
                    'package': xyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': xyz_data.info.paySign,
                    'success': function (res) {
                        _functionCYZ.CYZ_alert('购买成功', '/pages/user/index', 'tab');
                    },
                    'fail': function (res) {

                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //下拉刷新
    onPullDownRefresh: function () {
        var that = this;
        that.onShow();
        setTimeout(() => {
            wx.stopPullDownRefresh();
        }, 1000);
    },
});