const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        submitIsLoading: false,
        buttonIsDisabled: false,
        this_shop_config: null,
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopHehuo/index', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ this_shop_config: xyz_data.info.index_config });
            WxParse.wxParse('article', 'html', xyz_data.info.index_config.hehuo_shuoming, that, 5);
        });
    },
    add_formSubmit:function(e){
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        _functionCYZ.CYZ_loading_n("提交中，请稍候...");
        var requestData = e.detail.value;
        _requsetCYZ.cyz_requestGet('/ShopHehuo/shenqing', requestData, function (xyz_data) {
            wx.hideLoading();
            app.pushFormIdSubmit(e);
            if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert(xyz_data.info,"/pages/user/index",'tab');
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.add_formSubmit(e);
                });
                return false;
            } else {
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    go_mianze_info_bind: function () {
        wx.navigateTo({ url: "/pages/about/index?page_type=shop_hehuoren" });
    }
});