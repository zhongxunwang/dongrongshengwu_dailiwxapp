const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
  data: {
    this_c_config: null,
    select_type: 0,
    this_ad_list_data: null,
    this_cate_list_data: null,
    this_info_list_data: null,
    this_page_size: 1,
    this_page_num: 10,
    is_loadmore: true,
    this_config_commnet_show: false,
  },
  onLoad: function () {
    var that = this;
    that.setData({ this_c_config: app.globalData.app_config_data });
    that.getIndexData();
  },
  onShow: function () {
    
  },
  getIndexData: function () {
    var that = this;
    _functionCYZ.CYZ_loading();
    var requestData = {};
    requestData.pagesize = that.data.this_page_size;
    requestData.pagenum = that.data.this_page_num;
    _requsetCYZ.cyz_requestGet('/BusinessApi/getMyShoucang', requestData, function (xyz_data) {
      wx.hideToast();
      if (xyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          that.getIndexData();
        });
        return false;
      }
      if (xyz_data.info == null) {
        that.setData({ is_loadmore: false });
      } else {
        if (xyz_data.info.length < that.data.this_page_num) {
          that.setData({ is_loadmore: false });
        }
      }
      that.setData({ this_info_list_data: xyz_data.info });
    });
  },
  //取消收藏
  shoucang_info_bind:function(e){
    var that = this;
    wx.showModal({
      title: '提示',
      content: "确认要删除吗?",
      success: function (res) {
        if (res.confirm == true) {
          _functionCYZ.CYZ_loading_n("加载中");
          var requestData = {};
          requestData.shop_id = e.currentTarget.dataset.id;
          requestData.action_type = e.currentTarget.dataset.type;
          _requsetCYZ.cyz_requestGet('/BusinessApi/shopShoucang', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
              that.getIndexData();
              return true;
            } else if (xyz_data.code == 2) {
              app.getUserDataToken(function (token) {
                that.shoucang_info_bind(e);
              });
              return false;
            } else {
              _functionCYZ.CYZ_alert(xyz_data.info);
              return false;
            }
          });
        }
      }
    });
  },
  
  onReachBottom: function (e) {
    var that = this;
    if (that.data.is_loadmore == false) {
      return false;
    }
    _functionCYZ.CYZ_loading();
    var requestData = {};
    requestData.pagesize = that.data.this_page_size + 1;
    requestData.pagenum = that.data.this_page_num;
    _requsetCYZ.cyz_requestGet('/BusinessApi/getMyShoucang', requestData, function (xyz_data) {
      wx.hideToast();
      if (xyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          that.getIndexData();
        });
        return false;
      } else if (xyz_data.code == 1) {
        if (xyz_data.info == null) {
          that.setData({ is_loadmore: false });
        } else {
          if (xyz_data.info.length < that.data.this_page_num) {
            that.setData({ is_loadmore: false });
          }
          var this_new_info_data = that.data.this_info_list_data;
          this_new_info_data = this_new_info_data.concat(xyz_data.info);
          that.setData({ this_info_list_data: this_new_info_data, this_page_size: requestData.pagesize });
        }
      }
    });
  },
  /**
   * 跳转页面
   */
  onNavigateTap: function (e) {
    const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
    const nav = { url: url };
    if ("switch" == type) {
      nav.fail = function () {
        wx.navigateTo({ url: url });
      };
      wx.switchTab(nav);
    } else {
      wx.navigateTo(nav);
    }

  },
  /**
   * 预览视图
   */
  onPreviewTap: function (e) {
    var dataset = e.target.dataset, index = dataset.index, url = dataset.url;
    if (index === undefined && url === undefined) return;
    var urls = e.currentTarget.dataset.urls;
    urls = urls === undefined ? [] : urls;
    if (index !== undefined && !url) url = urls[index];
    wx.previewImage({ current: url, urls: urls });
  },
  /**
   * 拨打电话
   */
  onCallTap: function (e) {
    const dataset = e.currentTarget.dataset, mobile = dataset.mobile;
    wx.makePhoneCall({
      phoneNumber: mobile,
    });
  },
  onPullDownRefresh: function () {
    var that = this;
    that.setData({ this_page_size: 1, is_loadmore: true });
    that.getIndexData();
    setTimeout(() => {
      wx.stopPullDownRefresh()
    }, 1000);
  }
})