var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
  data: {
    score_arr: [
      {
        'val': 1,
        'ischeck': true
      },
      {
        'val': 2,
        'ischeck': true
      },
      {
        'val': 3,
        'ischeck': true
      },
      {
        'val': 4,
        'ischeck': true
      },
      {
        'val': 5,
        'ischeck': true
      }
    ],
    this_order_id: 0,
    submitIsLoading: false,
    buttonIsDisabled: false,
    this_score_val: 5,
    img_count_limit: 5,
    this_img_i: 0,
    this_img_max: 0,
    this_post_id: 0,
    postimg: []
  },
  onLoad: function (options) {
    var that = this
    var order_id = options.order_id;
    that.setData({
      this_order_id: order_id,
    })
  },
  //上传图片
  chooseimg_bind: function () {
    var that = this
    var img_lenth = that.data.postimg.length
    var sheng_count = that.data.img_count_limit - img_lenth
    if (sheng_count <= 0) {
      wx.showModal({
        title: '提示',
        content: '对不起，最多可上传五张图片',
        showCancel: false
      })
      return false
    }
    wx.chooseImage({
      count: sheng_count,
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          postimg: that.data.postimg.concat(res.tempFilePaths)
        })
      }
    })
  },
  //删除
  del_pic_bind: function (e) {
    var that = this
    var index = e.currentTarget.id;
    var datas = that.data.postimg;
    datas.splice(index, 1)
    that.setData({
      postimg: datas
    })
  },
  formSubmit: function (e) {
    var that = this
    var t_con = e.detail.value.content
    that.setData({
      submitIsLoading: true,
      buttonIsDisabled: true
    })
    if (t_con == '') {
      wx.showModal({
        title: '提示',
        content: '对不起，请输入评价内容',
        showCancel: false
      });
      that.setData({
        submitIsLoading: false,
        buttonIsDisabled: false
      })
      return false;
    }
    
    // 是否匿名评价
    _functionCYZ.CYZ_loading();
    var requestData = {};
    requestData.oid = that.data.this_order_id;
    requestData.fval = that.data.this_score_val;
    requestData.fcon = t_con;
    _requsetCYZ.cyz_requestGet('/ShopOrderApi/postComment', requestData, function (xyz_data) {
        wx.hideToast();
        if (xyz_data.code == 2) {
            app.getUserDataToken(function (token) {
                that.formSubmit(e);
            });
            return false;
        } else if (xyz_data.code == 1) {
            // 上传图片
            let filePathList = that.data.postimg;
            if (filePathList.length == 0) {
                that.initpostCommentOrderData();
            } else {
                that.imgUploadTime();
            }
        }else{
            that.setData({submitIsLoading: false,buttonIsDisabled: false});
            _functionCYZ.CYZ_alert(xyz_data.info);
            return false;
        }
    });
  },
  initpostCommentOrderData: function () {
        var that = this
        that.setData({
            submitIsLoading: false,
            buttonIsDisabled: false
        })
        wx.showModal({
            title: '提示',
            content: "订单评论成功",
            showCancel: false,
            success: function (res) {
                wx.redirectTo({
                    url: '../order/index'
                })
            }
        })
  },
  set_score_bind: function (e) {
    var that = this
    var max_val = e.currentTarget.id
    var datas = that.data.score_arr
    for (var i = 0; i < datas.length; i++) {
      if (i < max_val) {
        datas[i].ischeck = true
      } else {
        datas[i].ischeck = false
      }
    }
    that.setData({
      score_arr: datas,
      this_score_val: max_val
    })
  },
  imgUploadTime: function () {
      var that = this;
      var this_img_len = that.data.this_img_i;
      var this_img_max_len = that.data.postimg.length;
      if (this_img_len < this_img_max_len) {
          console.log(111)
          var requsetData = {};
          requsetData.order_id = that.data.this_order_id;
          _requsetCYZ.cyz_uploadPic('/ShopOrderApi/uploadInfoPic', that.data.postimg[this_img_len], requsetData, function (cdata) {
              that.setData({ this_img_i: that.data.this_img_i + 1 });
              that.imgUploadTime();
          });
      } else {
          wx.hideToast();
          that.initpostCommentOrderData();
      }
  },
})