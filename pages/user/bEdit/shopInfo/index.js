const app = getApp();
const _requsetCYZ = require('../../../../utils/requestData');
const _functionCYZ = require('../../../../utils/common');
var WxParse = require('../../../../wxParse/wxParse.js');
Page({
    /**
     * 页面的初始数据
     */
    data: {
        this_t_img: null,
        this_t_lng: 0,
        this_t_lat: 0,
        this_t_address: '',
        this_gps_info: '',
        this_shop_config: null,
        this_shop_cate: null,
        this_cate_index: 0,
        this_cate_id: 0,

        this_shop_info: null,
        this_shop_img:null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function() {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/BeditApi/getEditInfo', requestData, function(cyz_data) {
            wx.hideToast();
            that.setData({
                this_cate_id: cyz_data.info.info.shop_cate,
                this_shop_img: cyz_data.info.info.shop_logo,
                this_t_lng: cyz_data.info.info.shop_lng,
                this_t_lat: cyz_data.info.info.shop_lat,
                this_t_address: cyz_data.info.info.shop_address,
                this_shop_info: cyz_data.info.info,
                this_cate_index: cyz_data.info.info.this_cate_index,
                this_gps_info: cyz_data.info.info.shop_lat + ',' + cyz_data.info.info.shop_lng,
                this_shop_cate: cyz_data.info.shop_cate_list
            });
        });
    },
    //确认选择分类
    bindPickerChange: function(e) {
        this.setData({
            this_cate_id: this.data.this_shop_cate[e.detail.value].id,
            this_cate_index: e.detail.value
        });
    },
    // 选择商家坐标
    selectCoordinate: function(e) {
        this.setData({
            coorIndex: e.detail.value
        })
    },
    //选择图片
    chooseimg_bind: function() {
        var that = this;
        wx.chooseImage({
            count: 1,
            sizeType: ['compressed'],
            sourceType: ['album', 'camera'],
            success: function(res) {
                that.setData({
                    this_t_img: res.tempFilePaths
                })
            }
        })
    },
    //选择位置
    choose_t_map: function(e) {
        var that = this;
        wx.chooseLocation({
            success: function(res) {
                console.log(res);
                that.setData({
                    this_t_lat: res.latitude,
                    this_t_lng: res.longitude,
                    this_t_address: res.address,
                    this_gps_info: res.latitude + ',' + res.longitude
                })
            },
            error: function(res) {
                that.showModalTips('请允许地理位置授权');
            }
        })
    },
    //提交编辑
    addd_formSubmit: function(e) {
        var that = this;
        if (that.data.this_t_img == null || that.data.this_t_img == '') {
            if (that.data.this_shop_img == null || that.data.this_shop_img == '' ){
                that.showModalTips('请上传商家LOGO');
                return false;
            }
        }
        if (that.data.this_cate_id == 0) {
            that.showModalTips('请选择分类');
            return false;
        }
        that.setData({
            submitIsLoading: true,
            buttonIsDisabled: true
        });
        _functionCYZ.CYZ_loading_n("提交中，请稍候...");
        var requestData = e.detail.value;
        requestData.cate_id = that.data.this_cate_id;
        requestData.shop_address = that.data.this_t_address;
        requestData.shop_lat = that.data.this_t_lat;
        requestData.shop_lng = that.data.this_t_lng;
        _requsetCYZ.cyz_requestPost('/BeditApi/doEditInfo', requestData, function(xyz_data) {
            if (xyz_data.code == 1) {
                //开始上传图片
                that.imgUploadTime(xyz_data.info);
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function(token) {
                    that.addd_formSubmit(e);
                });
                return false;
            } else {
                wx.hideLoading();
                that.setData({buttonIsDisabled: false,submitIsLoading: false});
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    imgUploadTime: function(shop_id) {
        var that = this;
        if (that.data.this_t_img == null){
            that.go_confirm_index(shop_id);
        }else{
            var requestData = {};
            requestData.shop_id = shop_id;
            _requsetCYZ.cyz_uploadPic('/BusinessApi/uploadLogo', that.data.this_t_img[0], requestData, function (cdata) {
                that.go_confirm_index(shop_id);
            });
        }
        
    },
    showModalTips: function(msg, isShowCancel = false) {
        wx.showModal({
            title: '提示',
            content: msg,
            showCancel: isShowCancel,
            success: function(res) {
                return false;
            }
        });
    },
    //下拉刷新
    onPullDownRefresh: function() {
        var that = this;
        that.onLoad();
        setTimeout(() => {
            wx.stopPullDownRefresh();
        }, 1000);
    },
    //删除图片
    del_pic_bind_all: function(e) {
        var that = this
        var index = e.currentTarget.id;
        var datas = that.data.postimg;
        datas.splice(index, 1)
        that.setData({
            postimg: datas
        })
    },
    //上传图片
    chooseimg_bind_all: function() {
        var that = this;
        var img_lenth = that.data.postimg.length;
        var sheng_count = that.data.img_count_limit - img_lenth;
        if (sheng_count <= 0) {
            wx.showModal({
                title: '提示',
                content: '对不起，最多可上传' + that.data.img_count_limit + '张图片',
                showCancel: false
            })
            return false
        }
        wx.chooseImage({
            count: sheng_count,
            sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function(res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                that.setData({
                    postimg: that.data.postimg.concat(res.tempFilePaths)
                });
            }
        })
    },
    //上传图片
    submit_upload_pic: function(shop_id) {
        var that = this;
        var g_data = that.data.postimg;
        if (g_data.length > 0) {
            that.setData({
                this_img_max: g_data.length
            });
            that.imgUploadTime_all(shop_id);
        } else {
            that.go_confirm_index(shop_id);
        }
    },
    imgUploadTime_all: function(shop_id) {
        var that = this
        var this_img_len = that.data.this_img_i;
        var this_img_max_len = that.data.this_img_max;
        if (this_img_len < this_img_max_len) {
            var requestData = {};
            requestData.shop_id = shop_id;
            _requsetCYZ.cyz_uploadPic('/BusinessApi/uploadPics', that.data.postimg[this_img_len], requestData, function(cdata) {
                that.setData({
                    this_img_i: that.data.this_img_i + 1
                });
                that.imgUploadTime_all(shop_id);
            });
        } else {
            that.setData({
                postimg: [],
                this_img_i: 0,
                this_img_max: 0
            });
            that.go_confirm_index(shop_id);
        }
    },
    go_confirm_index: function() {
        wx.hideLoading();
        _functionCYZ.CYZ_alert('编辑成功', '/pages/user/index', 'tab');
    },
    //免责
    radioChange: function(e) {
        if (this.data.mianze_is_check == false) {
            this.setData({
                mianze_is_check: true
            });
        } else {
            this.setData({
                mianze_is_check: false
            });
        }
    },
    go_mianze_info_bind: function() {
        wx.navigateTo({
            url: "/pages/about/index?page_type=shop_shengming"
        });
    }
})