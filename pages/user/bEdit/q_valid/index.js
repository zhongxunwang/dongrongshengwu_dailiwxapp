var app = getApp();
var _requsetCYZ = require('../../../../utils/requestData');
var _functionCYZ = require('../../../../utils/common.js');

Page({
    data: {
        this_shop_data: null
    },
    formSubmit: function (e) {
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        _functionCYZ.CYZ_loading_n("验证中...");
        _requsetCYZ.cyz_requestPost('/ApiQuan/quanCancelCode', { exam_code: e.detail.value.exam_code}, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert('核销成功', "/pages/user/index", 'tab');
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.formSubmit(e);
                });
                return false;
            } else {
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    }
})