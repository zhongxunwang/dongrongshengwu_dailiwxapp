var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_shop_info:null
    },
    cacel_code_bind: function () {
        var that = this;
        wx.scanCode({
            success: (res) => {
                _functionCYZ.CYZ_loading_n("验证中...");
                _requsetCYZ.cyz_requestPost('/ApiQuan/quanCancelCode', { exam_code: res.result}, function (xyz_data) {
                    wx.hideLoading();
                    if (xyz_data.code == 1) {
                        app.commonErrorTips('核销成功');
                    } else if (xyz_data.code == 2) {
                        app.getUserDataToken(function (token) {
                            that.formSubmit(e);
                        });
                        return false;
                    } else {
                        app.commonErrorTips(xyz_data.info);
                        return false;
                    }
                });
            }
        })
    },
    onShow:function(){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/BeditApi/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
            } else if (xyz_data.code == 1) {
                WxParse.wxParse('article', 'html', xyz_data.info.s_config.shop_renzheng_tequan, that, 5);
                that.setData({ this_shop_info: xyz_data.info});
            }
        });
    }
})