var app = getApp();
var _requsetCYZ = require('../../../../utils/requestData');
var _functionCYZ = require('../../../../utils/common.js');

Page({
    data: {
        this_lunbo_data: null,
        this_t_img:null
    },
    onLoad:function(){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/BeditApi/editFocusInfo', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onLoad();
                });
            } else if (xyz_data.code == 1) {
                that.setData({ this_lunbo_data: xyz_data.info});
            }
        });
    },
    //选择图片
    chooseimg_bind: function () {
        var that = this;
        wx.chooseImage({
            count: 1,
            sizeType: ['compressed'],
            sourceType: ['album', 'camera'],
            success: function (res) {
                that.setData({this_t_img: res.tempFilePaths});
                _functionCYZ.CYZ_loading();
                var requestData = {};
                _requsetCYZ.cyz_uploadPic('/BeditApi/uploadPic', that.data.this_t_img[0], requestData, function (cdata) {
                    wx.hideToast();
                    that.onLoad();
                });
            }
        })
    },
    delete_pic_bind:function(e){
        var that = this;
        let p_id = e.currentTarget.dataset.id;
        wx.showModal({
            title: '提示',
            content: "确认要删除吗?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading();
                    var requestData = {};
                    requestData.p_id = p_id;
                    _requsetCYZ.cyz_requestGet('/BeditApi/delFocus', requestData, function (xyz_data) {
                        wx.hideToast();
                        if (xyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.delete_pic_bind(e);
                            });
                        } else if (xyz_data.code == 1) {
                            that.onLoad();
                        } else {
                            _functionCYZ.CYZ_alert(xyz_data.info);
                            return false;
                        }
                    });
                }
            }
        })
    }
})