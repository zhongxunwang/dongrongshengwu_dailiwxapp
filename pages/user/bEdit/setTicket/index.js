var app = getApp();
var _requsetCYZ = require('../../../../utils/requestData');
var _functionCYZ = require('../../../../utils/common.js');
Page({
    data: {
        q_type_v:'元',
        q_type_t:'优惠券面值',
        q_begin_time:'',
        q_end_time:'',
        q_yong_day: [
            {
                id: 1,
                name: '周一至周日'
            },
            {
                id: 2,
                name: '周一至周五'
            },
            {
                id: 3,
                name: '周六至周日'
            }],
        day_index:0,
        q_yong_time: [
            {
                id: 1,
                name: '全天'
            },
            {
                id: 2,
                name: '上午'
            },
            {
                id: 3,
                name: '下午'
            },
            {
                id: 4,
                name: '晚上'
            }
            ],
        time_index: 0
    },
    radioChange:function(e){
        if (e.detail.value == 1){
            this.setData({ q_type_v: '元', q_type_t:'优惠券面值'})
        }else{
            this.setData({ q_type_v: '折', q_type_t: '优惠券折扣' })
        }
    },
    bindDateChange_b:function(e){
        this.setData({ q_begin_time: e.detail.value});
    },
    bindDateChange_e: function (e) {
        this.setData({ q_end_time: e.detail.value });
    },
    change_day:function(e){
        this.setData({ day_index:e.detail.value});
    },
    change_time: function (e) {
        this.setData({ time_index: e.detail.value });
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ApiQuan/getQuanBase', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ q_begin_time: xyz_data.info.b_time, q_end_time: xyz_data.info.e_time});
        });
    },
    addd_formSubmit: function (e) {
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true});
        _functionCYZ.CYZ_loading_n("提交中，请稍候...");
        var requestData = e.detail.value;
        requestData.q_begin_time = that.data.q_begin_time;
        requestData.q_end_time = that.data.q_end_time;
        requestData.q_yong_day = that.data.day_index+1;
        requestData.q_yong_time = that.data.time_index + 1;
        _requsetCYZ.cyz_requestPost('/ApiQuan/quanAdd', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert('发布成功,请等待客服审核', "/pages/user/index", 'tab');
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.addd_formSubmit(e);
                });
                return false;
            } else {
                wx.hideLoading();
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
})