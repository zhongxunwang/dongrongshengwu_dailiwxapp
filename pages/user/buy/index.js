const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_c_config: null,
        this_config_info: null,
        this_user_account_info: null,
        c_val:10,
        btn_submit_disabled: false,
        submitIsLoading: false
    },
    onShow: function () {
        var that = this;
    },
    pay_formSubmit:function(e){
        var that = this;
        let c_val = e.detail.value.c_val;
        that.setData({c_val:c_val});
        if(c_val <= 0){
            _functionCYZ.CYZ_alert('请选择购买数量');
            return false;
        }
        that.setData({ btn_submit_disabled: true, submitIsLoading: true});
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.c_val = c_val;
        _requsetCYZ.cyz_requestGet('/Recharge/index', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ btn_submit_disabled: false, submitIsLoading: false});
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.pay_formSubmit(e);
                });
                return false;
            } else if (xyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': xyz_data.info.timeStamp,
                    'nonceStr': xyz_data.info.nonceStr,
                    'package': xyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': xyz_data.info.paySign,
                    'success': function (res) {
                        _functionCYZ.CYZ_alert('购买成功', '/pages/user/index', 'tab');
                    },
                    'fail': function (res) {
                       
                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    }
})