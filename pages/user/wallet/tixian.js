const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_shop_info_c: null,
        bank_list: null,
        picker_index: 0,
        bank_code: '',
        bank_name: '',
        this_cash_type: 0,
        this_user_data: null,
        btn_submit_disabled: false,
        submitIsLoading: false
    },
    onShow: function () {
        this.get_index_data();
    },
    select_account_type: function (e) {
        this.setData({ this_cash_type: e.currentTarget.id });
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/AppUserAccountApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_user_data: cyz_data.info,
                    this_shop_info_c: cyz_data.info.app_sys_config,
                    bank_list: cyz_data.info.this_bank_list
                });
                if (that.data.this_shop_info_c.cash_is_weixin == 1 && that.data.this_shop_info_c.cash_is_bank == 1) {
                    that.setData({ this_cash_type: 1 });
                } else if (that.data.this_shop_info_c.cash_is_weixin == 1 && that.data.this_shop_info_c.cash_is_bank == 0) {
                    that.setData({ this_cash_type: 1 });
                } else if (that.data.this_shop_info_c.cash_is_weixin == 0 && that.data.this_shop_info_c.cash_is_bank == 1) {
                    that.setData({ this_cash_type: 2 });
                }
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }

        });
    },
    cash_formSubmit: function (e) {
        var that = this;
        that.setData({ btn_submit_disabled: true, submitIsLoading: true });
        _functionCYZ.CYZ_loading();
        var requestData = e.detail.value;
        requestData.cash_type = that.data.this_cash_type;
        requestData.bank_code = that.data.bank_code;
        requestData.bank_name = that.data.bank_name;
        _requsetCYZ.cyz_requestPost('/AppUserAccountApi/sendCash', requestData, function (xyz_data) {
            console.log(xyz_data.info)
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.cash_formSubmit(e);
                });
                return false;
            } else if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert(xyz_data.info, '/pages/user/index', 'tab');
            } else if (xyz_data.code == 5) {
                that.setData({ btn_submit_disabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(xyz_data.info);
            }
        });
    },
    /**
     * 选择所属银行
     */
    changeBankList: function (e) {
        let index = e.detail.value;
        const bank_list = this.data.bank_list;
        this.setData({
            picker_index: index,
            bank_code: bank_list[index]['code'], // 银行code
            bank_name: bank_list[index]['name'], // 银行名称
        })
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
})