const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_data_now:null,
        this_data_list: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
    },
    onLoad: function () {
        var that = this;
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ShopShuju/getUserFanxianList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            if (xyz_data.info.list == null) {
                that.setData({is_loadmore: false});
            } else {
                if (xyz_data.info.list.length < that.data.this_page_num) {
                    that.setData({is_loadmore: false });
                }
            }
            that.setData({ this_data_list: xyz_data.info.list, this_data_now: xyz_data.info.fanxian_now});
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ShopShuju/getUserFanxianList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else if (xyz_data.code == 1) {
                if (xyz_data.info.list == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.list.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                    var this_new_info_data = that.data.this_data_list;
                    this_new_info_data = this_new_info_data.concat(xyz_data.info.list);
                    that.setData({ this_data_list: this_new_info_data, this_page_size: requestData.pagesize, this_data_now: xyz_data.info.fanxian_now});
                }
            }
        });
    },
    show_order_fanli_list:function(e){
        var that = this;
        let this_key = e.currentTarget.dataset.key;
        let this_list_data = that.data.this_data_list;
        this_list_data[this_key].o_fanli_isshow = this_list_data[this_key].o_fanli_isshow?false:true;
        that.setData({this_data_list: this_list_data});
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({ this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})