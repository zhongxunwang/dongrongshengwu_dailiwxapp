const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_app_sys_c:null,
        this_user_account: null
    },
    onShow: function () {
        this.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/AppUserAccountApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_user_account: cyz_data.info.user_account_info,
                    this_app_sys_c: cyz_data.info.app_sys_config
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }
        });
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url});
    },
    //转到余额
    jinbi_to_account_bind:function(){
        var that = this;
        wx.showModal({
            title: '提示',
            content: '确认将金币余额全部转入帐户余额吗',
            success: function (res) {
                if (res.confirm) {
                    _functionCYZ.CYZ_loading();
                    var requestData = {};
                    _requsetCYZ.cyz_requestGet('/AppUserAccountApi/jinToAccount', requestData, function (cyz_data) {
                        wx.hideToast();
                        if (cyz_data.code == 1) {
                            that.get_index_data();
                        } else if (cyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.jinbi_to_account_bind();
                            });
                            return false;
                        }else{
                            _functionCYZ.CYZ_alert(cyz_data.info);
                            return false;
                        }
                    });
                }
            }
        })
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
})