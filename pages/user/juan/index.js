var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_quan_list: [],
        this_select_type:0
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.quan_status = that.data.this_select_type;
        _requsetCYZ.cyz_requestGet('/ShopApi/getUserQuanList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
            } else if (xyz_data.code == 1) {
                that.setData({ this_quan_list: xyz_data.info });
            }
        });
    },
    change_select_type:function(e){
        let t_type = e.currentTarget.id;
        this.setData({ this_select_type:t_type});
        this.onShow();
    }
})