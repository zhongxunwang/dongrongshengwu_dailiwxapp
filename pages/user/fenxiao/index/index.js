const _requsetCYZ = require('../../../../utils/requestData');
const _functionCYZ = require('../../../../utils/common');
const app = getApp();
Page({
    data: {
        this_user_info: null
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopFenxiaoApi/getMyFenxiaoBase', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_user_info: cyz_data.info});
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info,"/pages/user/shenqingheduoren/index",'nav');
                return false;
            }
        });
    },
})