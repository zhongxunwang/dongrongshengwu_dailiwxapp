const _requsetCYZ = require('../../../../utils/requestData');
const _functionCYZ = require('../../../../utils/common');
var WxParse = require('../../../../wxParse/wxParse.js');
const app = getApp();
Page({
    data: {
        this_user_info: null,
        this_shop_config:null
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopFenxiaoApi/getFenxiaoCode', requestData, function (cyz_data) {
            wx.hideToast();
            console.log(cyz_data)
            if (cyz_data.code == 1) {
                that.setData({ this_user_info: cyz_data.info });
                //获取商城配置
                var requestData = {};
                _requsetCYZ.cyz_requestGet('/ShopApi/getShopConfig', requestData, function (cyz_data) {
                    that.setData({ this_shop_config: cyz_data.info });
                    WxParse.wxParse('article', 'html', cyz_data.info.fenxiao_shuoming, that, 5);
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    save_img_tomy_bind:function(){
        _functionCYZ.CYZ_loading();
        let img_path = this.data.this_user_info.user_rcode;
        wx.getImageInfo({
            src: img_path,
            success:function(res){
                let tmp_p = res.path;
                wx.saveImageToPhotosAlbum({
                    filePath: tmp_p,
                    success:function(){
                        wx.hideToast();
                        _functionCYZ.CYZ_alert('保存成功');
                    },
                    fail:function(){
                        wx.hideToast();
                        _functionCYZ.CYZ_alert('图片信息获取失败');
                    }
                })
            },
            fail:function(){
                wx.hideToast();
                _functionCYZ.CYZ_alert('图片信息获取失败');
            }
        })
    }
})