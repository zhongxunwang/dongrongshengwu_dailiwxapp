const _requsetCYZ = require('../../../../utils/requestData');
const _functionCYZ = require('../../../../utils/common');
const app = getApp();
Page({
    data: {
        this_data_list: null,
        this_data_count:null,
        group_val: 1,
        this_select_type:0
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        let base_url = '';
        if (that.data.group_val == 1){
            base_url = '/AgentUserFenxiaoApi/myFenxiaoTeamList';
        }else{
            base_url = '/AgentUserFenxiaoApi/myFenxiaoTeamList_two';
        }
        requestData.select_type = that.data.this_select_type;
        _requsetCYZ.cyz_requestGet(base_url, requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ 
                    this_data_list: cyz_data.info.one_data_list,
                    this_data_count: cyz_data.info.count_data
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    group_show:function(e){
        let f_level = e.currentTarget.dataset.val;
        this.setData({group_val: f_level});
        this.onShow();
    },
    change_select_type:function(e){
        let f_level = e.currentTarget.dataset.val;
        this.setData({ this_select_type: f_level });
        this.onShow();
    }
})