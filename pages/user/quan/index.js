var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');

Page({
    data: {
        this_page: 1,
        this_select_type: -1,
        this_quan_data: null,
        is_show_img: 0,
        this_qr_code_img: null,
        this_exam_code: '********',
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        is_show_code: false,
        this_code_data: null
    },
    /**
     * 分类筛选
     */
    change_select_type_bind: function(e) {
        this.setData({
            this_select_type: e.currentTarget.dataset.id
        });
        this.init_select_param();
        this.onShow();
    },
    /**
     * 重置搜索条件
     */
    init_select_param: function() {
        this.setData({
            this_page: 1
        });
    },
    onShow: function() {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.select_type = that.data.this_select_type;
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ApiQuanUser/getMyQuanList', requestData, function(cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 2) {
                app.getUserDataToken(function(token) {
                    that.onShow();
                });
                return false;
            }
            if (cyz_data.info == null) {
                that.setData({
                    is_loadmore: false
                });
            } else {
                if (cyz_data.info.length < that.data.this_page_num) {
                    that.setData({
                        is_loadmore: false
                    });
                }
            }
            that.setData({
                this_quan_data: cyz_data.info
            });
        });
    },
    onReachBottom: function(e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.select_type = that.data.this_select_type;
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ApiQuanUser/getMyQuanList', requestData, function(xyz_data) {
            wx.hideToast();
            if (xyz_data.info == null) {
                that.setData({
                    is_loadmore: false
                });
            } else {
                if (xyz_data.info.length < that.data.this_page_num) {
                    that.setData({
                        is_loadmore: false
                    });
                }
                var this_new_info_data = that.data.this_quan_data;
                this_new_info_data = this_new_info_data.concat(xyz_data.info);
                that.setData({
                    this_quan_data: this_new_info_data,
                    this_page_size: requestData.pagesize
                });
            }
        });
    },
    //切换规则
    change_guize_status: function(e) {
        let that = this;
        let this_index = e.currentTarget.dataset.index;
        let this_quan_data = that.data.this_quan_data;
        this_quan_data[this_index].gz_isshow = this_quan_data[this_index].gz_isshow ? false : true;
        that.setData({
            this_quan_data: this_quan_data
        });
    },
    //生成核销码
    makeCancleCode: function(e) {
        var that = this;
        let code_id = e.currentTarget.dataset.id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.code_id = code_id;
        _requsetCYZ.cyz_requestGet('/ApiQuanUser/makeCancelCode', requestData, function(xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function(token) {
                    that.makeCancleCode(e);
                });
            } else if (xyz_data.code == 1) {
                that.setData({
                    is_show_code: true,
                    this_code_data: xyz_data.info
                });
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
            }
        });
    },
    close_img_bind:function(){
        this.setData({ is_show_code:false});
    },
    onPullDownRefresh: function() {
        var that = this;
        that.init_select_param();
        that.onShow();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})