var app = getApp();
var _requsetData = require('../../../utils/data');
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        
    },
    onUserInfo: function (e) {
        var that = this;
        console.log(e);
        const detail = e.detail;
        if (!detail.userInfo) {
            _functionCYZ.CYZ_alert('请同意授权');
        } else {
            var that = this;
            var info = detail.userInfo;
            _functionCYZ.CYZ_loading();
            var requestData = {};
            requestData.u_data = JSON.stringify(info);
            _requsetCYZ.cyz_requestPost('/User/updateUserInfo', requestData, function (xyz_data) {
                wx.hideToast();
                if (xyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.onUserInfo(e);
                    });
                } else if (xyz_data.code == 1){
                    wx.navigateBack();
                }else{
                    _functionCYZ.CYZ_alert('授权失败，请重试授权');
                }
            });
        }
    },
    //新版本登陆
	getUserProfile: function (e) {
		let _this = this;
		let _this_eeeee = e;
		wx.getUserProfile({
			lang: 'zh_CN',
			desc: '完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
			success: (res) => {
				var requestData = {};
				requestData.code = _this.data.this_user_code;
				requestData.user_info = JSON.stringify(res.userInfo);
				_requsetCYZ.cyz_requestPost('/WeixinMpLogin/updateUserInfo', requestData, function (res) {
					if (res.code == 1) {
                        wx.setStorageSync('is_auth_login_back', 1);
				        wx.navigateBack();
					} else if (res.code == 2) {
						app.getUserDataToken(function (token) {
							_this.getUserProfile(_this_eeeee);
						});
						return false;
					}else{
						_functionCYZ.CYZ_alert(res.info);
					}
				});
			},
			fail: function (res) {
				_functionCYZ.CYZ_loading('授权登陆失败，请返回重试', 'none', 1000);
			}
		})
	}
})