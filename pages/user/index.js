var app = getApp();
var _requsetCYZ = require('../../utils/requestData');
var _functionCYZ = require('../../utils/common.js');
Page({
    data: {
        this_config_info: null,
        this_card_info: null,
        this_shop_info:null
    },
    onShow: function () {
        var that = this;
        app.checkUserGIsName();
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/UserInfo/userInfo', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
            } else if (xyz_data.code == 1) {
                that.setData({ this_card_info: xyz_data.info.user_info, this_config_info: xyz_data.info.c_config, this_shop_info: xyz_data.info.shop_info });
            }
        });
    },
    go_shop_action:function(){
        var that = this;
        if (that.data.this_shop_info.shop_s_action == 1){
            wx.navigateTo({ url: "/pages/business_enter/index" });
        } else if (that.data.this_shop_info.shop_s_action == 2){//审核中
            wx.showToast({
                title: '店铺审核中，请耐心等待',
                icon: 'none',
                duration: 1000
            })
            return false;
        } else if (that.data.this_shop_info.shop_s_action == 3) {
            wx.navigateTo({ url: "bEdit/index" });
        }
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    //充值
    go_recharge_bind: function () {
        wx.navigateTo({ url: "/pages/user/recharge/index" });
    },
    go_my_info: function () {
        wx.navigateTo({ url: "/pages/info/my-lists" });
    },
    go_adinfo_bind: function () {
        wx.navigateTo({ url: "/pages/about/index?page_type=adinfo" });
    },
    go_lianxiinfo_bind: function () {
        wx.navigateTo({ url: "/pages/about/index?page_type=contactus" });
    },
    go_shoucang_info: function () {
        wx.navigateTo({ url: "/pages/user/infodata/my_shoucang" });
    },
    call_copy_bind: function () {
        wx.makePhoneCall({
            phoneNumber: this.data.this_card_info.copy_phone || '15516169228',
        });
    },
    //刷新用户信息
    onUserInfo: function (e) {
        var that = this;
        const detail = e.detail;
        if (!detail.userInfo) {
            _functionCYZ.CYZ_alert('请同意授权');
        } else {
            var that = this;
            var info = detail.userInfo;
            _functionCYZ.CYZ_loading();
            var requestData = {};
            requestData.u_data = JSON.stringify(info);
            _requsetCYZ.cyz_requestPost('/User/updateUserInfo', requestData, function (xyz_data) {
                wx.hideToast();
                if (xyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.onUserInfo(e);
                    });
                } else if (xyz_data.code == 1) {
                    wx.showToast({
                        title: '信息同步成功',
                        duration: 2000,
                        success:function(){
                            setTimeout(function () {
                                that.onShow();
                            }, 2000);
                        }
                    });
                    
                } else {
                    _functionCYZ.CYZ_alert('授权失败，请重试授权');
                }
            });
        }
    },
    //新版本登陆
    getUserProfile: function (e) {
        let _this = this;
        let _this_eeeee = e;
        wx.getUserProfile({
            lang: 'zh_CN',
            desc: '完善会员资料',
            success: (res) => {
                _functionCYZ.CYZ_loading();
                var requestData = {};
                requestData.u_data = JSON.stringify(res.userInfo);
                _requsetCYZ.cyz_requestPost('/User/updateUserInfo', requestData, function (xyz_data) {
                    wx.hideToast();
                    if (xyz_data.code == 2) {
                        app.getUserDataToken(function (token) {
                            _this.getUserProfile(_this_eeeee);
                        });
                    } else if (xyz_data.code == 1) {
                        wx.showToast({
                            title: '信息同步成功',
                            duration: 2000,
                            success: function () {
                                setTimeout(function () {
                                    _this.onShow();
                                }, 2000);
                            }
                        });

                    } else {
                        _functionCYZ.CYZ_alert('授权失败，请重试授权');
                    }
                });
            },
            fail: function (res) {
                _functionCYZ.CYZ_loading('授权登陆失败，请返回重试', 'none', 1000);
            }
        })
    },
})