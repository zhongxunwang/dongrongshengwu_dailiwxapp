const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_info_id: 0,
        this_info_data: null,
        btn_submit_disabled: false,
        submitIsLoading: false
    },
    onShow: function () {
        var that = this;
    },
    info_formSubmit: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading_n("加载中");
        that.setData({ btn_submit_disabled: true, submitIsLoading: true });
        var requestData = e.detail.value;
        _requsetCYZ.cyz_requestGet('/InfoAction/app_jianyi', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert(xyz_data.info, '/pages/user/index', 'tab');
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.info_formSubmit(e);
                });
                return false;
            } else {
                that.setData({ btn_submit_disabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    }
})