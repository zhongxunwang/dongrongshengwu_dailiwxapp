var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_user_info:null,
        this_share_info:null
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/Tg/getShareConfig', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ this_user_info: xyz_data.info.user_info, this_share_info: xyz_data.info.share_info});
            wx.setNavigationBarTitle({
                title: xyz_data.info.share_info.title||'分享'
            });
            WxParse.wxParse('article', 'html', xyz_data.info.share_info.share_info, that, 5);
        });
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_share_info.title;
        var sharePath = 'pages/info/index?tg_user_id=' + that.data.this_user_info.id;
        //share_big_img
        return {
            title: shareTitle,
            imageUrl: that.data.this_share_info.share_big_img||'',
            desc: '',
            path: sharePath
        }
    },
})
