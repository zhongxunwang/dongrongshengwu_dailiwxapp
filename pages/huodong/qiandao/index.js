var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        config_info:null,
        this_user_data:null
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ApiQiandao/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            }
            that.setData({ config_info: xyz_data.info.this_config_info, this_user_data: xyz_data.info.user_data});
            wx.setNavigationBarTitle({
                title: xyz_data.info.this_config_info.title
            });
        });
    },
    go_qiandao_bind:function(){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ApiQiandao/qiandao', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_qiandao_bind();
                });
                return false;
            } else if (xyz_data.code == 5) {
                _functionCYZ.CYZ_alert(xyz_data.info);
            } else if (xyz_data.code == 1) {
                wx.showModal({
                    title: '提示',
                    content: xyz_data.info,
                    showCancel: false,
                    success: function (res) {
                        that.onShow();
                    }
                });
            }
        });
    }
})
