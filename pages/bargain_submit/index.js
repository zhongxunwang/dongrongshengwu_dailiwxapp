const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
Page({
    data: {
        this_goods_id: 0,
        this_goods_info:null,
        this_pro_info:null,
        this_peisong_type:0,
        this_address_id: 0,
        this_address_info: '请选择',
        wx_address_info: null,
        this_all_price:0
    },
    chang_peisong_type: function (e) {
        var that = this;
        let ptype = e.currentTarget.dataset.tval;
        if(ptype == 1){
            that.setData({ this_all_price: parseFloat(that.data.this_pro_info.deal_money) + parseFloat(that.data.this_goods_info.g_yunfei)});
        } else if (ptype == 2){
            that.setData({ this_all_price: parseFloat(that.data.this_pro_info.deal_money)});
        }
        this.setData({ this_peisong_type: ptype });
    },
    onLoad: function (op) {
        var that = this;
        let goods_id = 0;
        goods_id = op.goods_id;
        that.setData({this_goods_id: goods_id});
        that.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        _requsetCYZ.cyz_requestGet('/ApiBargainOrder/getUserProgressInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                
                let all_price = 0;
                if (cyz_data.info.index_goods_info.g_is_peisong == 1){
                    all_price = parseFloat(cyz_data.info.index_pro_info.deal_money) + parseFloat(cyz_data.info.index_goods_info.g_yunfei);
                }else{
                    all_price = cyz_data.info.index_pro_info.deal_money;
                }
                that.setData({
                    this_goods_info: cyz_data.info.index_goods_info,
                    this_pro_info: cyz_data.info.index_pro_info,
                    this_peisong_type: cyz_data.info.index_goods_info.this_peisong_type,
                    this_all_price: all_price
                });
                that.getAddressInfo();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info, "/pages/quan/index", "tab");
                return false;
            }
        });
    },
    onShow: function () {
        this.getAddressInfo();
    },
    //选择收货地址
    select_address_bind: function () {
        var that = this;
        wx.navigateTo({
            url: '/pages/user/adress/index'
        });
        return false;
    },
    //获取用户收货地址
    getAddressInfo: function () {
        var that = this;
        var aid = wx.getStorageSync("shop_select_address_id");
        if (aid && aid > 0) {
            _functionCYZ.CYZ_loading();
            var requestData = {};
            requestData.fapiao_id = aid;
            _requsetCYZ.cyz_requestGet('/ApiAddress/getAddresssInfo', requestData, function (cyz_data) {
                wx.hideToast();
                if (cyz_data.code == 1) {
                    var wx_address = {};
                    wx_address.userName = cyz_data.info.consignee;
                    wx_address.telNumber = cyz_data.info.mobile;
                    wx_address.detailInfo = cyz_data.info.address + cyz_data.info.buchong;
                    wx_address.u_lat = cyz_data.info.u_lat;
                    wx_address.u_lng = cyz_data.info.u_lng;
                    that.setData({ wx_address_info: wx_address });
                } else if (cyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.getAddressInfo();
                    });
                    return false;
                } else {
                    _functionCYZ.CYZ_alert(cyz_data.info);
                    return false;
                }
            });
        }
    },
    //提交订单
    order_formSubmit: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading();
        that.setData({ btn_submit_disabled: true, submitIsLoading: true });
        var order_info = e.detail.value;
        order_info.wx_address = JSON.stringify(that.data.wx_address_info);
        order_info.form_id = e.detail.formId;
        order_info.goods_id = that.data.this_goods_id;
        order_info.peisong_type = that.data.this_peisong_type;
        _requsetCYZ.cyz_requestPost('/ApiBargainOrder/postOrder', order_info, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': cyz_data.info.timeStamp,
                    'nonceStr': cyz_data.info.nonceStr,
                    'package': cyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': cyz_data.info.paySign,
                    'success': function (res) {
                        wx.showModal({
                            title: '提示',
                            content: "订单支付成功",
                            confirmText: "查看订单",
                            showCancel: false,
                            success: function (res) {
                                wx.navigateTo({
                                    url: "/pages/user/order/index",
                                });
                            }
                        });
                    },
                    'fail': function (res) {
                        that.setData({ btn_submit_disabled: false, submitIsLoading: false });
                    },
                    'complete': function () {
                        that.setData({ btn_submit_disabled: false, submitIsLoading: false });
                    }
                });
                return false;
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.order_formSubmit(e);
                });
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
            }
            that.setData({ btn_submit_disabled: false, submitIsLoading: false });
        });
    },
})