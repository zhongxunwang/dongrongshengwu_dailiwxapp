const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        cate_data: null,
        info:null,
        childCates: null
    },
    write_info_bind:function(e){
        wx.navigateTo({ url: 'write?name=' + e.currentTarget.dataset.name + '&cid=' + e.currentTarget.dataset.cid });
    },
    onShow: function () {
        var that = this;
        app.checkUserGIsName();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/Info/getCateGoryList', requestData, function (xyz_data) {
            const group = [];
            var i = -1;
            for (var x in xyz_data.info) {
                if (x % 4 === 0) {
                    group[++i] = [];
                }
                group[i].push(xyz_data.info[x]);
            }
            that.setData({ cate_data: group });

            var requestData = {};
            requestData.page_type = 'info_shengming';
            _requsetCYZ.cyz_requestGet('/Page/about', requestData, function (xyz_data) {
                wx.hideToast();
                that.setData({ info: xyz_data.info });
                WxParse.wxParse('article', 'html', xyz_data.info.page_content, that, 5);
            });
        });
    },
    /**
     * 选择分类
     */
    onChooseCateTap: function (e) {
        var that = this;
        const dataset = e.detail.target.dataset, gIndex = dataset.gIndex, index = dataset.index;
        const curCate = this.data.cate_data[gIndex][index], childCates = curCate.two_list;
        if (childCates && childCates.length) {
            this.setData({ childCates });
        } else {
            wx.navigateTo({ url: 'write?name=' + curCate.title + '&cid=' + curCate.id + '&c_type=' + curCate.c_type });
            that.onChooseCancel();
        }
    },

	/**
	 * 选择子分类
	 */
    onChooseChildCateTap: function (e) {
        var that = this;
        const dataset = e.currentTarget.dataset, index = dataset.index;
        const childCates = this.data.childCates, item = childCates[index];
        wx.navigateTo({ url: 'write?name=' + item.title + '&cid=' + item.id + '&c_type=' + item.c_type });
        that.onChooseCancel();
    },

	/**
	 * 选择被取消
	 */
    onChooseCancel: function () {
        this.setData({ childCates: null });
    }
})