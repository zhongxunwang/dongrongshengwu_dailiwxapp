const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
const QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');// 引入SDK核心类
const qqmapsdk = new QQMapWX({ key: 'YDUBZ-ZHGC5-D65I6-QKTDH-PBM6E-SGF2M' });// 实例化API核心类
Page({
  data: {
    this_info_id: 0,
    this_info_data: null,
    btn_submit_disabled: false,
    submitIsLoading: false
  },
  onLoad: function (options) {
    this.setData({ this_info_id: options.info_id });
  },
  onShow: function () {
    var that = this;
  },
  info_formSubmit:function(e){
    var that = this;
    _functionCYZ.CYZ_loading_n("加载中");
    that.setData({ btn_submit_disabled: true, submitIsLoading: true });
    var requestData = e.detail.value;
    requestData.info_id = that.data.this_info_id;
    _requsetCYZ.cyz_requestGet('/InfoAction/jubao', requestData, function (xyz_data) {
      wx.hideLoading();
      if (xyz_data.code == 1) {
        _functionCYZ.CYZ_alert(xyz_data.info, '/pages/info/index', 'tab');
      } else if (xyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          that.info_formSubmit(e);
        });
        return false;
      } else {
        that.setData({ btn_submit_disabled: false, submitIsLoading: false });
        _functionCYZ.CYZ_alert(xyz_data.info);
        return false;
      }
    });
  }
})