const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
Page({
    data: {
        this_info_id: 0,
        this_info_data: null,
        this_config_commnet_show: false,
        latitude: 0,
        longitude: 0,
		this_one_ad_data: null,
		this_two_ad_data: null,
		this_login_user_id:0,
		this_config_op:null,

        this_isshow_lad:false,
        this_lad_data:null
    },
    onLoad: function (options) {
        this.setData({this_info_id: options.id,this_config_op: options});
    },
    getLocation: function () {
        var that = this;
        _functionCYZ.CYZ_loading_n("定位中，请稍候...");
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                that.setData({ latitude: res.latitude, longitude: res.longitude });
                that.getInfoData();
            },
            fail: function () {
                //弹出系统设置
                wx.openSetting({
                    success: (res) => {
                        if (res.authSetting['scope.userLocation'] == false) {
                            wx.showModal({
                                title: '提示',
                                content: "请允许地理位置授权",
                                showCancel: false,
                                success: function () {
                                    that.getLocation();
                                }
                            });
                        } else {
                            that.getLocation();
                        }
                    }
                });
                return false;
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    },
    onShow: function () {
        var that = this;
		let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
                if (wx.getStorageSync("global_user_address")) {
                    that.setData({
                        latitude: wx.getStorageSync("global_user_lat"),
                        longitude: wx.getStorageSync("global_user_lng")
                    });
                    that.getInfoData();
                } else {
                    that.getLocation();
                }
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            }
        });
    },
    getInfoData:function(){
        var that = this;
        _functionCYZ.CYZ_loading_n("加载中");
        var requestData = {};
        requestData.info_id = that.data.this_info_id;
        requestData.u_lat = that.data.latitude;
        requestData.u_lng = that.data.longitude;
        _requsetCYZ.cyz_requestGet('/Info/getInfoDetail', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.setData({ this_info_data: xyz_data.info });
                //加载系统配置 是否显示评论等功能
                _requsetCYZ.cyz_requestGet('/Index/getConfigOthersValue', {}, function (xyz_data) {
                    that.setData({ this_config_commnet_show: xyz_data.info.commnet_is_open, this_app_title: xyz_data.info.app_top_title });
                });
				//获取详情页广告图
				_requsetCYZ.cyz_requestGet('/Info/getDetailAdList', {}, function (xyz_data) {
                    that.setData({ this_one_ad_data: xyz_data.info.ad_one_list, this_two_ad_data: xyz_data.info.ad_two_list, this_lad_data: xyz_data.info.liuliang_ad, this_isshow_lad:true});
				});
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getInfoData();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info, "index", "tab");
                return false;
            }
        });
    },
    onPreviewTap: function (e) {
        //预览视图
        var dataset = e.target.dataset, index = dataset.index, url = dataset.url;
        if (index === undefined && url === undefined) return;
        var urls = e.currentTarget.dataset.urls;
        urls = urls === undefined ? [] : urls;
        if (index !== undefined && !url) url = urls[index];
        wx.previewImage({ current: url, urls: urls });
    },
    /**
     * 拨打电话
     */
    bind_tap_lianxi: function () {
        wx.makePhoneCall({
            phoneNumber: this.data.this_info_data.mobile,
        });
    },
	//广告跳转
	ad_goinfo_bind: function (e) {
        console.log(e);
		if (e.currentTarget.id && e.currentTarget.id > 0) {
			wx.navigateTo({ url: e.currentTarget.id });
		}
	},
	goHome_bind:function(){
		wx.switchTab({ url: "/pages/info/index"});
	},
    //导航
    bind_tap_daohang:function(){
      var that = this;
      var loc_lat = that.data.this_info_data.latitude;
      var loc_lng = that.data.this_info_data.longitude;
      wx.openLocation({
        latitude: parseFloat(loc_lat),
        longitude: parseFloat(loc_lng),
        scale: 18,
        address: that.data.this_info_data.address
      });
    },
    //举报
    bind_tap_jubao:function(){
      wx.navigateTo({ url: "jubao?info_id=" + this.data.this_info_data.id });
    },
    /**
     * 显示评论框
     */
    onShowCommentTap: function (e) {
        console.log(e)
        const dataset = e.currentTarget.dataset.docId != undefined ? e.currentTarget.dataset : e.target.dataset;
        const values = {
            index: e.currentTarget.dataset.index,
            comment_index: dataset.commentIndex,
            reply_id: dataset.replyId,
            reply_uid: dataset.uid,
            doc_id: dataset.docId,
        };

        if (!values.reply_id && !values.doc_id) return;
        const comment_placeholder = (values.reply_id ? "回复 " : "评论 ") + dataset.nickname;
        this.setData({ commentParam: values, show_comment: true, comment_placeholder: comment_placeholder });
    },
    /**
     * 隐藏评论框
     */
    onHideCommentTap: function () {
        this.setData({ show_comment: false });
    },
    /**
     * 提交评论
     */
    onCommentSubmit: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading_n("请稍候...");
        var requestData = e.detail.value;
        requestData.formId = e.detail.formId;
        requestData.commentParam = that.data.commentParam;
        _requsetCYZ.cyz_requestGet('/InfoAction/commentAdd', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.getInfoData();
                that.setData({ show_comment: false });
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onCommentSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    /**
     * 赞
     */
    onGoodTap: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading_n("加载中");
        var requestData = {};
        requestData.info_id = e.currentTarget.dataset.id;
        requestData.action_type = e.currentTarget.dataset.type;
        _requsetCYZ.cyz_requestGet('/InfoAction/goodUpdate', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.getInfoData();
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onGoodTap(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.getInfoData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_info_data.share_title;
        var sharePath = 'pages/info/detail?id=' + that.data.this_info_data.id+'&tg_user_id='+that.data.this_login_user_id;
        console.log(sharePath)
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
    /**
	 * 抢红包
	 */
    onPullWalletSubmit: function () {
        var that = this;
        _functionCYZ.CYZ_loading_n("加载中");
        var requestData = {};
        requestData.info_id = that.data.this_info_data.id;
        _requsetCYZ.cyz_requestGet('/InfoAction/lingHongbao', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.getInfoData();
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onPullWalletSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    /**
	 * 点击设置值
	 */
    onSetValueTap(e) {
        const dataset = e.target.dataset, name = dataset.name;
        let value = dataset.value, info = {};
        if (name) {
            info[name] = value;
        } else {
            info = JSON.parse(value);
        }
        this.setData(info);
    },
})