const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
const QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');// 引入SDK核心类
const qqmapsdk = new QQMapWX({ key: 'YDUBZ-ZHGC5-D65I6-QKTDH-PBM6E-SGF2M' });// 实例化API核心类
Page({
    data: {
		this_app_muchcity_open: false,
		this_app_muchcity_select: 2,
		sdk_user_address_info: [],
        this_c_config: null,
        this_cate_id:0,
        this_cate_name:'全部',
        this_s_keywords:'',
        select_type: 0,
        this_u_lat: 0,
        this_u_lng: 0,
        this_app_title: '同城信息',
        this_user_city: '全国',
        this_ad_list_data: null,
        this_cate_list_data: null,
        this_old_cate_list:null,
        this_info_list_data: null,
		this_two_ad_data: null,
        commentParam: [],
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        this_user_select_city: '',
        select_city_layer_status: false,
        this_config_commnet_show: false,
        this_show_juli: false,
        two_cate_list: null,
        childCid: 0
    },
    //选择分类显示
    select_cate_bind: function () {
        this.setData({ is_show_catelist: this.data.is_show_catelist ? false : true });
    },
    change_cate_bind: function (e) {
        this.setData({ this_cate_id: e.currentTarget.dataset.id, this_page_size: 1, this_cate_name: e.currentTarget.dataset.name });
        this.setData({ is_show_catelist: false, is_loadmore: true });
        this.getIndexData();
    },
    select_city_bind: function (e) {
        var that = this;
        var this_c_name = e.currentTarget.dataset.name;
        var this_c_field = e.currentTarget.dataset.field;
        that.setData({ this_city_field: this_c_field, this_user_select_city: this_c_name });
        that.setData({ this_city_parent_level: e.currentTarget.id });
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.parent_level = that.data.this_city_parent_level;
        _requsetCYZ.cyz_requestGet('/Index/getCityList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.info == null) {
                that.set_alert_content_confrim();
                return false;
            }
            that.setData({ this_city_data: xyz_data.info, select_city_layer_status: true });
        });
    },
    set_alert_content_setno: function () {
        var that = this;
        this.set_alert_content_close();
    },
    //关闭城市
    set_alert_content_close: function () {
        this.setData({ this_city_data: [], select_city_layer_status: false, this_city_parent_level: 1, this_city_field: 1 });
    },
    //确认选择
    set_alert_content_confrim: function () {
        var that = this;
        that.setData({ this_city_data: [], select_city_layer_status: false, this_city_parent_level: 1, this_city_field: 1, this_page_size: 1, is_loadmore: true, this_s_keywords: that.data.this_s_keywords, this_user_city: that.data.this_user_select_city });
        wx.setNavigationBarTitle({
            title: that.data.this_app_title + '-' + that.data.this_user_select_city
        });
        that.getIndexData();
    },
    //搜索
    info_search_bind: function (e) {
        this.setData({ this_page_size: 1, is_loadmore: true, this_s_keywords: e.detail.value });
        this.getIndexData();
    },
    //自动定位
    user_location_bind: function () {
        this.getLocation();
    },
    //发布信息
    add_info_bind: function () {
        wx.switchTab({ url: 'release' });
    },
    /**
	 * 加载二级分类数据
	 */
    onLoadChooseCateTap: function (e) {
        const dataset = e.detail.target.dataset, cid = dataset.cid;
        this.setData({ childCid: cid });
        this.setData({ this_page_size: 1, is_loadmore: true });
        this.getIndexData();
    },
    onLoad: function (options) {
        var that = this;
        that.setData({ this_c_config: app.globalData.app_config_data, this_cate_id: options.cid, this_cate_name:options.title||'全部'});
        wx.setNavigationBarTitle({title: options.title});
		if (wx.getStorageSync('global_user_address')) {
			that.getSystemConfig();
        } else {
            this.getLocation();
        }
    },
    onShow: function () {
        //this.getIndexData();
    },
    getLocation: function () {
        var that = this;
        _functionCYZ.CYZ_loading_n("定位中，请稍候...");
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    success: (res) => {
						that.setData({ sdk_user_address_info: res.result });
                        wx.setStorageSync('global_user_city', res.result.address_component.city);
                        wx.setStorageSync('global_user_address', res.result.address);
                        wx.setStorageSync('global_user_lat', res.result.location.lat);
                        wx.setStorageSync('global_user_lng', res.result.location.lng);
                        wx.setStorageSync('global_user_province', res.result.address_component.province);
                        wx.setStorageSync('global_user_city', res.result.address_component.city);
                        wx.setStorageSync('global_user_district', res.result.address_component.district);
						that.getSystemConfig();
                        that.set_alert_content_close();
                    }
                });
            },
            fail: function () {
                //弹出系统设置
                wx.openSetting({
                    success: (res) => {
                        if (res.authSetting['scope.userLocation'] == false) {
                            wx.showModal({
                                title: '提示',
                                content: "请允许地理位置授权",
                                showCancel: false,
                                success: function () {
                                    that.getLocation();
                                }
                            });
                        } else {
                            that.getLocation();
                        }
                    }
                });
                return false;
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    },
	//获取系统配置
	getSystemConfig: function () {
		var that = this;
		//加载系统配置 是否显示评论等功能
		_requsetCYZ.cyz_requestGet('/Index/getConfigOthersValue', {}, function (xyz_data) {
			that.setData({ this_config_commnet_show: xyz_data.info.commnet_is_open, this_app_title: xyz_data.info.app_top_title, this_app_muchcity_open: xyz_data.info.app_muchcity_open });
			//如果缓存存在
			var user_city_fanwei = "全国";
			if (wx.getStorageSync('global_user_address')) {
				if (xyz_data.info.app_muchcity_select == 1) {
					user_city_fanwei = wx.getStorageSync("global_user_province");
				} else if (xyz_data.info.app_muchcity_select == 2) {
					user_city_fanwei = wx.getStorageSync("global_user_city");
				} else if (xyz_data.info.app_muchcity_select == 3) {
                    user_city_fanwei = wx.getStorageSync("global_user_district");
				}
				that.setData({
					this_user_city: user_city_fanwei,
					this_u_lat: wx.getStorageSync("global_user_lat"),
					this_u_lng: wx.getStorageSync("global_user_lng")
				});
			} else {
				if (xyz_data.info.app_muchcity_select == 1) {
					user_city_fanwei = that.data.sdk_user_address_info.address_component.province;
				} else if (xyz_data.info.app_muchcity_select == 2) {
					user_city_fanwei = that.data.sdk_user_address_info.address_component.city;
				} else if (xyz_data.info.app_muchcity_select == 3) {
					user_city_fanwei = that.data.sdk_user_address_info.address_component.district;
				}
				that.setData({
					this_user_city: user_city_fanwei,
					this_u_lat: that.data.sdk_user_address_info.location.lat,
					this_u_lng: that.data.sdk_user_address_info.location.lng
				});
			}
            if (xyz_data.info.app_muchcity_open) {
                wx.setNavigationBarTitle({
                    title: xyz_data.info.app_top_title + '-' + that.data.this_user_city
                });
            } else {
                wx.setNavigationBarTitle({
                    title: xyz_data.info.app_top_title
                });
            }
			that.getIndexData();
		});
	},
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        requestData.cate_id = that.data.this_cate_id;
        requestData.cate_two_id = that.data.childCid;
        requestData.keywords = that.data.this_s_keywords;
        requestData.u_city = that.data.this_user_city;
        requestData.select_type = that.data.select_type;
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/Info/getIndexData', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            if (xyz_data.info.info_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.info_list.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({ this_info_list_data: xyz_data.info.info_list, this_two_ad_data: xyz_data.info.ad_two_list, this_old_cate_list: xyz_data.info.cate_list,two_cate_list: xyz_data.info.two_cate_list});
            console.log(that.data.two_cate_list);
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        requestData.cate_id = that.data.this_cate_id;
        requestData.cate_two_id = that.data.childCid;
        requestData.keywords = that.data.this_s_keywords;
        requestData.u_city = that.data.this_user_city;
        requestData.select_type = that.data.select_type;
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/Info/getIndexData', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else if (xyz_data.code == 1) {
                if (xyz_data.info.info_list == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.info_list.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                    var this_new_info_data = that.data.this_info_list_data;
                    this_new_info_data = this_new_info_data.concat(xyz_data.info.info_list);
                    that.setData({ this_info_list_data: this_new_info_data, this_page_size: requestData.pagesize });
                }
            }
        });
    },

    onSwtchTabTap: function (e) {
        var that = this;
        const dataset = e.currentTarget.dataset, index = dataset.index;
        this.setData({ this_page_size: 1, is_loadmore: true, select_type: index });
        if (index == 1) {
            this.setData({ this_show_juli: true });
            if (wx.getStorageSync("global_user_address")) {
                that.getIndexData();
            } else {
                that.getLocation();
            }
        } else {
            this.getIndexData();
            this.setData({ this_show_juli: false });
        }
    },
	//广告跳转
	ad_goinfo_bind: function (e) {
		if (e.currentTarget.id && e.currentTarget.id > 0) {
			wx.navigateTo({ url: "detail?id=" + e.currentTarget.id });
		}
	},
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }

    },
    /**
     * 预览视图
     */
    onPreviewTap: function (e) {
        var dataset = e.target.dataset, index = dataset.index, url = dataset.url;
        if (index === undefined && url === undefined) return;
        var urls = e.currentTarget.dataset.urls;
        urls = urls === undefined ? [] : urls;
        if (index !== undefined && !url) url = urls[index];
        wx.previewImage({ current: url, urls: urls });
    },
    /**
     * 拨打电话
     */
    onCallTap: function (e) {
        const dataset = e.currentTarget.dataset, mobile = dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    /**
     * 赞
     */
    onGoodTap: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading_n("加载中");
        var requestData = {};
        requestData.info_id = e.currentTarget.dataset.id;
        _requsetCYZ.cyz_requestGet('/InfoAction/goodUpdate', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.getIndexData();
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onGoodTap(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    /**
     * 显示评论框
     */
    onShowCommentTap: function (e) {
        const dataset = e.target.dataset;
        const values = {
            index: e.currentTarget.dataset.index,
            comment_index: dataset.commentIndex,
            reply_id: dataset.replyId,
            reply_uid: dataset.uid,
            doc_id: dataset.docId,
        };
        if (!values.reply_id && !values.doc_id) return;
        const comment_placeholder = (values.reply_id ? "回复 " : "评论 ") + dataset.nickname;
        this.setData({ commentParam: values, show_comment: true, comment_placeholder: comment_placeholder });
    },
    /**
     * 隐藏评论框
     */
    onHideCommentTap: function () {
        this.setData({ show_comment: false });
    },
    /**
     * 提交评论
     */
    onCommentSubmit: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading_n("请稍候...");
        var requestData = e.detail.value;
        requestData.formId = e.detail.formId;
        requestData.commentParam = that.data.commentParam;
        _requsetCYZ.cyz_requestGet('/InfoAction/commentAdd', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.setData({ show_comment: false });
                wx.navigateTo({ url: 'detail?id=' + xyz_data.info });
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onCommentSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({ this_s_keywords: '', this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})