const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
Page({
    data: {
        this_jinbi_pay_open: 0,
        this_c_config: null,
        this_config_info:null,
        select_type: 0,
        this_ad_list_data: null,
        this_cate_list_data: null,
        this_info_list_data: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        this_config_commnet_show: false,
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/UserInfo/userInfo', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
            } else if (xyz_data.code == 1) {
                that.setData({ this_card_info: xyz_data.info.user_info, this_config_info: xyz_data.info.c_config});
            }
        });
        this.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/Info/getMyList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            if (xyz_data.info.info_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.info_list.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({this_info_list_data: xyz_data.info.info_list });
            //加载系统配置 是否显示评论等功能
            _requsetCYZ.cyz_requestGet('/Index/getConfigOthersValue', {}, function (xyz_data) {
                that.setData({ this_config_commnet_show: xyz_data.info.commnet_is_open });
            });
        });
    },
    //删除信息
    delete_info_bind:function(e){
        var that = this;
        var info_id = e.target.id;
        wx.showModal({
            title: '提示',
            content: "确认要删除该信息?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading('请稍候');
                    var requestData = {};
                    requestData.info_id = info_id;
                    _requsetCYZ.cyz_requestGet('/UserInfo/deleteMyInfo', requestData, function (xyz_data) {
                        wx.hideToast();
                        if (xyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.delete_info_bind(e);
                            });
                        } else if (xyz_data.code == 1) {
                            _functionCYZ.CYZ_loading(xyz_data.info, 'loading', 1000);
                            that.onPullDownRefresh();
                        } else {
                            _functionCYZ.CYZ_alert(xyz_data.info);
                            return false;
                        }
                    });
                }
            }
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/Info/getMyList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else if (xyz_data.code == 1) {
                if (xyz_data.info.info_list == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.info_list.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                    var this_new_info_data = that.data.this_info_list_data;
                    this_new_info_data = this_new_info_data.concat(xyz_data.info.info_list);
                    that.setData({ this_info_list_data: this_new_info_data, this_page_size: requestData.pagesize });
                }
            }
        });
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }

    },
    /**
     * 预览视图
     */
    onPreviewTap: function (e) {
        var dataset = e.target.dataset, index = dataset.index, url = dataset.url;
        if (index === undefined && url === undefined) return;
        var urls = e.currentTarget.dataset.urls;
        urls = urls === undefined ? [] : urls;
        if (index !== undefined && !url) url = urls[index];
        wx.previewImage({ current: url, urls: urls });
    },
    /**
     * 拨打电话
     */
    onCallTap: function (e) {
        const dataset = e.currentTarget.dataset, mobile = dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    /**
     * 显示评论框
     */
    onShowCommentTap: function (e) {
        const dataset = e.target.dataset;
        const values = {
            index: e.currentTarget.dataset.index,
            comment_index: dataset.commentIndex,
            reply_id: dataset.replyId,
            reply_uid: dataset.uid,
            doc_id: dataset.docId,
        };
        if (!values.reply_id && !values.doc_id) return;
        const comment_placeholder = (values.reply_id ? "回复 " : "评论 ") + dataset.nickname;
        this.setData({ commentParam: values, show_comment: true, comment_placeholder: comment_placeholder });
    },
    /**
     * 隐藏评论框
     */
    onHideCommentTap: function () {
        this.setData({ show_comment: false });
    },
    /**
     * 提交评论
     */
    onCommentSubmit: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading_n("请稍候...");
        var requestData = e.detail.value;
        requestData.formId = e.detail.formId;
        requestData.commentParam = that.data.commentParam;
        _requsetCYZ.cyz_requestGet('/InfoAction/commentAdd', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                that.setData({ show_comment: false });
                wx.navigateTo({ url: 'detail?id=' + xyz_data.info });
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onCommentSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    go_pay_bind: function (e) {
        var that = this;
        var info_id = e.currentTarget.id;
        if (that.data.this_config_info.tongcheng_is_open_jinbi == 1) {
            //显示余额支付
            wx.showActionSheet({
                itemList: ['金币支付', '微信支付'],
                success: (res) => {
                    if (res.tapIndex == 0) {
                        that.go_jinbi_pay_bind(info_id);
                    } else if (res.tapIndex == 1) {
                        //微信支付
                        that.go_pay_wx_bind(info_id);
                    }
                }, fail: () => {
                    //跳转我的发布
                    wx.redirectTo({ url: 'my-lists', });
                }
            });
            return false;
        } else {
            //微信支付
            that.go_pay_wx_bind(info_id);
            return false;
        }

        
    },
    go_pay_wx_bind:function(info_id){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.info_id = info_id;
        _requsetCYZ.cyz_requestGet('/Info/infoPay', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_pay_wx_bind(info_id);
                });
            } else if (xyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': xyz_data.info.timeStamp,
                    'nonceStr': xyz_data.info.nonceStr,
                    'package': xyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': xyz_data.info.paySign,
                    'success': function (res) {
                        wx.setStorageSync('index_auto_loading', 1);
                        _functionCYZ.CYZ_alert('信息支付成功', '/pages/info/index', 'tab');
                    },
                    'fail': function (res) {

                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    go_jinbi_pay_bind: function (info_id) {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.info_id = info_id;
        _requsetCYZ.cyz_requestGet('/Info/infoJinbiPayJinbi', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_jinbi_pay_bind(info_id);
                });
            } else if (xyz_data.code == 1) {
                wx.setStorageSync('index_auto_loading', 1);
                _functionCYZ.CYZ_alert('信息支付成功', '/pages/info/index', 'tab');
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
})