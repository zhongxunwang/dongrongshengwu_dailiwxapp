var app = getApp();
var _requsetCYZ = require('../../utils/requestData');
var _functionCYZ = require('../../utils/common.js');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
		this_page_type:'about',
        info: null
    },
	onLoad:function(op){
		if (op.page_type != undefined){
			this.setData({ this_page_type: op.page_type});
		}
	},
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
		requestData.page_type = that.data.this_page_type;
		_requsetCYZ.cyz_requestGet('/Page/about', requestData, function (xyz_data) {
            wx.hideToast();
			console.log(xyz_data.info)
            that.setData({ info: xyz_data.info });
			wx.setNavigationBarTitle({
				title: "招商加盟"
			});
            WxParse.wxParse('article', 'html', xyz_data.info.page_content, that, 5);
        });
    }
})
