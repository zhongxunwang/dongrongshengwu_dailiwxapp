const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
Page({
    data: {
        this_cate_list: [],
        tabIndex: 0,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        this_index_data: null,
        this_s_title: '',
        isShowCates:false,
        this_config_op:null,
        this_login_user_id: 0
    },
    openCates:function(){
        this.setData({ isShowCates: this.data.isShowCates?false:true});
    },
    onLoad: function (op) {
        var that = this;
        that.setData({this_config_op: op });
        that.getSystemData();
    },
    getSystemData: function () {
        var that = this;
        let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getSystemData();
                });
                return false;
            }
        });
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        if (wx.getStorageSync("this_cms_cate_id")) {
            that.setData({ tabIndex: wx.getStorageSync("this_cms_cate_id") });
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.cate_id = that.data.tabIndex;
        requestData.s_key = that.data.this_s_title;
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/BigCmsApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            that.setData({this_cate_list: cyz_data.info.index_cate_list });
            if (cyz_data.info.index_list_data == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.index_list_data.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            wx.setStorageSync('this_cms_cate_id', null);
            that.setData({ this_index_data: cyz_data.info.index_list_data });
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.cate_id = that.data.tabIndex;
        requestData.s_key = that.data.this_s_title;
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/BigCmsApi/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.info.index_list_data == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.index_list_data.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
                var this_new_info_data = that.data.this_index_data;
                this_new_info_data = this_new_info_data.concat(xyz_data.info.index_list_data);
                that.setData({ this_index_data: this_new_info_data, this_page_size: requestData.pagesize });
            }
        });
    },
    //切换选项卡
    onTabChangeTap: function (e) {
        const that = this;
        var index = e.currentTarget.dataset.index;
        that.setData({ tabIndex: index, is_loadmore: true, this_s_title: '', this_page_size: 1 });
        that.getIndexData();//加载数据
        that.setData({ isShowCates:false});
    },
    //搜索
    changeSearchStatus: function () {
        this.setData({ search_open_status: this.data.search_open_status ? false : true });
    },
    //搜索
    searchTitle: function (e) {
        this.setData({ this_s_title: e.detail.value });
    },
    onSearch: function (e) {
        var that = this;
        that.setData({ is_load_more: true, tabIndex: 0, this_page_size: 1 });
        that.onShow();//加载数据
    },
    // 关闭搜索
    closeSearch: function (e) {
        var that = this;
        if (that.data.this_s_title != '') {
            that.setData({ tabIndex: 0, is_loadmore: true, this_s_title: '', this_page_size: 1 });
            that.onShow();//加载数据
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({ this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    go_single_bind: function (e) {
        let tid = e.currentTarget.id;
        wx.navigateTo({ url: "../single-news/index?id=" + tid });
    },
    onNavigateTap: function (e) {
        //跳转页面
        wx.navigateTo({
            url: e.currentTarget.dataset.url
        })
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = '看资讯';
        var sharePath = 'pages/bigcms/index?tg_user_id=' + that.data.this_login_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})