const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_op: null,
        this_id:0,
        this_data_info: null,
        this_config_op: null,
        this_login_user_id: 0
    },
    onLoad: function (op) {
        var that = this;
        that.setData({ this_id:op.id,this_config_op: op });
        that.getSystemData();
    },
    getSystemData: function () {
        var that = this;
        let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getSystemData();
                });
                return false;
            }
        });
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        let tid = that.data.this_id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.id = tid;
        _requsetCYZ.cyz_requestGet('/BigCmsApi/getArticleInfo', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 1) {
                that.setData({ this_data_info: xyz_data.info });
                WxParse.wxParse('article', 'html', xyz_data.info.content, that, 5);
            } else {
                wx.navigateBack();
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_data_info.title;
        var sharePath = 'pages/bigcms/info?id=' + that.data.this_data_info.id + '&tg_user_id=' + that.data.this_login_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
    /**
     * 跳转页面
     */
    onAppNavigateTap: function (e) {
        const dataset = e.detail.target ? e.detail.target.dataset : e.currentTarget.dataset;
        const url = dataset.url, type = dataset.type, nav = { url: url }, appId = dataset.appId;
        wx.navigateTo({
            url: url, fail: () => {
                wx.switchTab({
                    url: url,
                });
            }
        });
    },
})