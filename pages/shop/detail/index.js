const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_shop_config_c: null,
        this_g_nav: 1,
        this_goods_id: 0,
        this_shop_id: 0,
        this_goods_info: null,
        goods_specification: [],
        goods_add_card_show: false,
        cart_default_number: 1,
        goods_attr_select: {},
        btn_add_cart_disabled: false,
        scrollTop: 0,
        floorstatus: true,
        is_add_cart_view: false,
        is_buy_now_view: false,
        this_login_user_id: 0,
        shop_attr_price: [],
        is_show_code:false
    },
    show_wxcode_bind:function(){
        this.setData({is_show_code: this.data.is_show_code?false:true});
    },
    /**
     * 跳转页面
     */
    onAppNavigateTap: function (e) {
        const dataset = e.detail.target ? e.detail.target.dataset : e.currentTarget.dataset;
        const url = dataset.url, appurl = dataset.appurl, atype = dataset.atype || 1, appId = dataset.appid;
        if (atype == 1) {
            wx.navigateTo({
                url: url, fail: () => {
                    wx.switchTab({
                        url: url,
                    });
                }
            });
        } else if (atype == 2) {
            wx.navigateToMiniProgram({ appId: appId, path: appurl });
        }
    },
    goTop: function (e) {
        this.setData({
            scrollTop: 0
        })
    },
    onLoad: function (op) {
        var that = this;
        let goods_id = 0;
        if (op.goods_id != undefined) {
            goods_id = op.goods_id;
        } else if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            goods_id = scene || 0;
        }
        that.setData({ this_goods_id: goods_id });
        if (op.tg_user_id != undefined) {
            let t_user_id = op.tg_user_id;
            //添加推广
            var requestData = {};
            requestData.top_user_id = t_user_id || 0;
            _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
                if (cyz_data.code == 1) {
                    that.get_index_data();
                } else if (cyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.onLoad(op);
                    });
                }
            });
        } else {
            that.get_index_data();
        }
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        _requsetCYZ.cyz_requestGet('/ShopApi/getGoodsInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_goods_info: cyz_data.info, this_shop_id: cyz_data.info.shop_id, this_login_user_id: cyz_data.info.login_user_id, goods_specification: cyz_data.info.goods_specification, this_shop_config_c: cyz_data.info.shop_info_c });
                if (cyz_data.info != null) {
                    wx.setNavigationBarTitle({
                        title: cyz_data.info.g_name
                    });
                    WxParse.wxParse('article', 'html', cyz_data.info.g_description, that, 5);
                }
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }
        });
    },
    goods_nav_bind: function (e) {
        this.setData({ this_g_nav: e.currentTarget.id });
    },
    //返回商城首页
    go_shop_index_bind: function () {
        wx.switchTab({
            url: '../shop_index/index'
        })
    },
    //图片放大
    img_max_bind: function (e) {
        var that = this;
        var img_max_url = e.currentTarget.dataset.url;
        var this_img_key = e.currentTarget.dataset.key;
        var all_img_num = that.data.this_goods_info.cmlist[this_img_key].com_imglist.length;
        var durls = [];
        for (var i = 0; i < all_img_num; i++) {
            durls[i] = that.data.this_goods_info.cmlist[this_img_key].com_imglist[i].imgurl;
        }
        wx.previewImage({
            current: img_max_url,
            urls: durls
        })
    },
    //图片预览
    preview_look_img: function (e) {
        wx.previewImage({
            current: '',
            urls: [e.currentTarget.dataset.url]
        })
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
  
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
})