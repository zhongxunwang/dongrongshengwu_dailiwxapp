// pages/components/coFastNav/coFastNav.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
      isShow:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
      setDataTap:function(){
          this.setData({ isShow: this.data.isShow?false:true});
      },
      linkToTap:function(e){
          wx.navigateTo({
              url: e.currentTarget.dataset.url,
              fail: function () {
                  wx.switchTab({ url: e.currentTarget.dataset.url });
              }
          });
      }
  }
})
