const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_data_list:null,
        this_copy_info:null
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ApiHelp/index', requestData, function (cyz_data) {
            wx.hideToast();
            that.setData({ this_data_list: cyz_data.info.help_list, this_copy_info: cyz_data.info.copy_info});
        });
    },
    changStatus:function(e){
        var that = this;
        let t_index = e.currentTarget.dataset.index;
        let t_data_list = that.data.this_data_list;
        if (t_data_list[t_index].is_active == 1){
            t_data_list[t_index].is_active = 0;
        }else{
            for (var i = 0; i < t_data_list.length;i++){
                t_data_list[i].is_active=0;
            }
            t_data_list[t_index].is_active =1;
        }
        that.setData({ this_data_list: t_data_list });
    },
    call_copy_bind: function () {
        wx.makePhoneCall({
            phoneNumber: this.data.this_copy_info.daili_contact_phone || '15516169228',
        });
    },
    //复制微信
    get_weixin_bind: function () {
        var that = this;
        wx.setClipboardData({
            data: that.data.this_copy_info.daili_contact_weixin || '',
            success: function (res) {
                wx.showToast({
                    title: '微信号已复制',
                    icon: 'success',
                    duration: 2000
                })
            }
        })
    },
})