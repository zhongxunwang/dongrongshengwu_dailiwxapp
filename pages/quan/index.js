const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
const QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');// 引入SDK核心类
const qqmapsdk = new QQMapWX({ key: 'WFABZ-MNEW3-JWC3B-YNHD3-XKVM3-4OBY2' });// 实例化API核心类
Page({
    data: {
        this_app_muchcity_open: false,
        this_app_muchcity_select: 2,
        this_cate_list: null,
        this_config_data: null,
        this_shop_data: null,
        this_ad_list: null,
        this_ruzhu_new_data: null,
        this_ruzhu_rmp_data: null,
        this_shop_list: null,
        this_cate_id: 0,
        this_s_keywords: '',
        select_type: 0,
        this_u_lat: 0,
        this_u_lng: 0,
        this_user_city: '全国',
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        this_user_select_city: '',
        select_city_layer_status: false,
        this_show_juli: false,
        is_alert_content_t: false,
        is_close_in: false,

        this_config_op:null,
        this_login_user_id:0,
        this_quan_list:null,
        this_quan_cate_list:null,
        this_bargain_list:null,
        this_bargain_index:1,
        this_bargain_allIndex:0
    },
    //砍价滚动
    change_bargin_index: function (e) {
        this.setData({
            this_bargain_index: e.detail.current + 1
        });
    },
    //切换选项卡
    openCates: function () {
        this.setData({ isShowCates: this.data.isShowCates ? false : true });
    },
    onTabChangeTap: function (e) {
        const that = this;
        var index = e.currentTarget.dataset.index;
        that.setData({ this_cate_id: index, is_loadmore: true, this_s_title: '', this_page_size: 1 });
        that.getIndexData();
        that.setData({ isShowCates: false });
    },
    set_alert_content_close_t: function () {
        this.setData({ is_alert_content_t: false, is_close_in: true });
    },
    onNavigateTap: function (e) {
        this.setData({ this_cate_id: e.currentTarget.dataset.cid, this_page_size: 1 });
        this.getIndexData();
    },
    select_city_bind: function (e) {
        var that = this;
        var this_c_name = e.currentTarget.dataset.name;
        var this_c_field = e.currentTarget.dataset.field;
        that.setData({ this_city_field: this_c_field, this_user_select_city: this_c_name });
        that.setData({ this_city_parent_level: e.currentTarget.id });
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.parent_level = that.data.this_city_parent_level;
        _requsetCYZ.cyz_requestGet('/Index/getCityList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.info == null) {
                that.set_alert_content_confrim();
                return false;
            }
            that.setData({ this_city_data: xyz_data.info, select_city_layer_status: true });
        });
    },
    set_alert_content_setno: function () {
        var that = this;
        this.set_alert_content_close();
    },
    //关闭城市
    set_alert_content_close: function () {
        this.setData({ this_city_data: [], select_city_layer_status: false, this_city_parent_level: 1, this_city_field: 1 });
    },
    //确认选择
    set_alert_content_confrim: function () {
        var that = this;
        that.setData({ this_city_data: [], select_city_layer_status: false, this_city_parent_level: 1, this_city_field: 1, this_page_size: 1, is_loadmore: true, this_s_keywords: that.data.this_s_keywords, this_user_city: that.data.this_user_select_city });
        that.getIndexData();
    },
    //自动定位
    user_location_bind: function () {
        this.getLocation();
    },
    //搜索
    info_search_bind: function (e) {
        this.setData({ this_page_size: 1, is_loadmore: true, this_s_keywords: e.detail.value });
        this.getIndexData();
    },
    getLocation: function () {
        var that = this;
        _functionCYZ.CYZ_loading("定位中，请稍候...");
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    success: (res) => {
                        that.setData({ sdk_user_address_info: res.result });
                        wx.setStorageSync('global_user_city', res.result.address_component.city);
                        wx.setStorageSync('global_user_address', res.result.address);
                        wx.setStorageSync('global_user_lat', res.result.location.lat);
                        wx.setStorageSync('global_user_lng', res.result.location.lng);
                        wx.setStorageSync('global_user_province', res.result.address_component.province);
                        wx.setStorageSync('global_user_city', res.result.address_component.city);
                        wx.setStorageSync('global_user_district', res.result.address_component.district);
                        that.getSystemData();
                        that.set_alert_content_close();
                    }
                });
            },
            fail: function () {
                //弹出系统设置
                wx.openSetting({
                    success: (res) => {
                        if (res.authSetting['scope.userLocation'] == false) {
                            wx.showModal({
                                title: '提示',
                                content: "请允许地理位置授权",
                                showCancel: false,
                                success: function () {
                                    that.getLocation();
                                }
                            });
                        } else {
                            that.getLocation();
                        }
                    }
                });
                return false;
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    },
    onSwtchTabTap: function (e) {
        var that = this;
        const dataset = e.currentTarget.dataset, index = dataset.index;
        this.setData({ this_page_size: 1, is_loadmore: true, select_type: index });
        if (index == 1) {
            if (wx.getStorageSync("global_user_address")) {
                that.getIndexData();
            } else {
                that.getLocation();
            }
        } else {
            this.getIndexData();
        }
    },
    onShow: function () {

    },
    onLoad: function (op) {
        var that = this;
        that.setData({ this_config_op: op });
        that.setData({ this_cate_id: 0, this_s_keywords: '', this_page_size: 1 });
        if (wx.getStorageSync('global_user_address')) {
            that.getSystemData();
        } else {
            that.getLocation();
        }
    },
    getSystemData: function () {
        var that = this;
        let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getSystemData();
                });
                return false;
            }
        });
        //加载系统配置
        _requsetCYZ.cyz_requestGet('/Index/getConfigOthersValue', {}, function (xyz_data) {
            that.setData({ this_app_muchcity_open: xyz_data.info.app_muchcity_open });
            //如果缓存存在
            var user_city_fanwei = "全国";
            if (wx.getStorageSync('global_user_address')) {
                if (xyz_data.info.app_muchcity_select == 1) {
                    user_city_fanwei = wx.getStorageSync("global_user_province");
                } else if (xyz_data.info.app_muchcity_select == 2) {
                    user_city_fanwei = wx.getStorageSync("global_user_city");
                } else if (xyz_data.info.app_muchcity_select == 3) {
                    user_city_fanwei = wx.getStorageSync("global_user_district");
                }
                that.setData({
                    this_user_city: user_city_fanwei,
                    this_u_lat: wx.getStorageSync("global_user_lat"),
                    this_u_lng: wx.getStorageSync("global_user_lng")
                });
            } else {
                if (xyz_data.info.app_muchcity_select == 1) {
                    user_city_fanwei = that.data.sdk_user_address_info.address_component.province;
                } else if (xyz_data.info.app_muchcity_select == 2) {
                    user_city_fanwei = that.data.sdk_user_address_info.address_component.city;
                } else if (xyz_data.info.app_muchcity_select == 3) {
                    user_city_fanwei = that.data.sdk_user_address_info.address_component.district;
                }
                that.setData({
                    this_user_city: user_city_fanwei,
                    this_u_lat: that.data.sdk_user_address_info.location.lat,
                    this_u_lng: that.data.sdk_user_address_info.location.lng
                });
            }

            if (xyz_data.info.app_muchcity_open == false) {
                that.setData({
                    this_user_city: '全国'
                });
            }
            that.getIndexData();
        });
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        requestData.keywords = that.data.this_s_keywords;
        requestData.cate_id = that.data.this_cate_id;
        requestData.u_city = that.data.this_user_city;
        requestData.select_type = that.data.select_type;
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/ApiQuan/getQuanList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info.quan_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.quan_list.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false});
                }
            }
            that.setData({
                this_ad_list: cyz_data.info.index_ad_lunbo,
                this_config_data: null,
                this_quan_list: cyz_data.info.quan_list,
                this_quan_cate_list: cyz_data.info.quan_cate_list,
                this_bargain_list: cyz_data.info.index_bargain_list,
            });
            if (cyz_data.info.index_bargain_list != null){
                that.setData({
                    this_bargain_allIndex: cyz_data.info.index_bargain_list.length
                });
            }
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        requestData.cate_id = that.data.this_cate_id;
        requestData.keywords = that.data.this_s_keywords;
        requestData.u_city = that.data.this_user_city;
        requestData.select_type = that.data.select_type;
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/ApiQuan/getQuanList', requestData, function (xyz_data) {
            wx.hideToast();

            if (xyz_data.info.shop_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.shop_list.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
                var this_new_info_data = that.data.this_shop_list;
                this_new_info_data = this_new_info_data.concat(xyz_data.info.shop_list);
                that.setData({ this_shop_list: this_new_info_data, this_page_size: requestData.pagesize });
            }

        });
    },
    /**
	 * 拨打电话
	 */
    onCallTap: function (e) {
        const mobile = e.currentTarget.dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    //广告链接跳转
    go_ad_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    //地图跳转
    go_map_info_b: function () {
        var that = this;
        var t_address = that.data.this_cms_config.cms_address;
        _requsetCYZ.cyz_requestGet('/Map/getLocation', { address: t_address }, function (xyz_data) {
            wx.openLocation({
                latitude: parseFloat(xyz_data.info.lat),
                longitude: parseFloat(xyz_data.info.lng),
                scale: 18,
                address: t_address
            });
        });
    },
    //领取优惠券
    quan_lingqu_bind:function(e){
        var that = this;
        let this_quan_data = that.data.this_quan_list;
        let quan_id = e.currentTarget.dataset.id || 0;
        let t_index = e.currentTarget.dataset.index || 0;
        _requsetCYZ.cyz_requestGet('/ApiQuanUser/userLingQuan', { quan_id: quan_id }, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                this_quan_data[t_index].user_is_ling = 1;
                that.setData({ this_quan_list: this_quan_data});
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.quan_lingqu_bind(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = "点击领取本地商家优惠券";
        var sharePath = 'pages/quan/index?tg_user_id=' + that.data.this_login_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})