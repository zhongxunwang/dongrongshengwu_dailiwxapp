const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_config_op: null,
        this_data_info: null,
        this_login_user_id:0
    },
    onLoad: function (op) {
        var that = this;
        that.setData({ this_config_op: op });
        that.getSystemData();
    },
    getSystemData: function () {
        var that = this;
        let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getSystemData();
                });
                return false;
            }
        });
        that.get_index_data();
    },
    get_index_data:function(){
        var that = this;
        let op = that.data.this_config_op;
        var requestData = {};
        //添加推广
        requestData.top_user_id = 0;
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            if (op.scene > 0) {
                requestData.top_user_id = scene || 0;
            }
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                requestData.top_user_id = op.tg_user_id || 0;
            }
        }
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info });
                that.get_data_info();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }
        });
    },
    get_data_info:function(){
        var that = this;
        let op = that.data.this_config_op;
        let tid = op.quan_id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.quan_id = tid;
        _requsetCYZ.cyz_requestGet('/ApiQuan/getQuanInfo', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 1) {
                that.setData({ this_data_info: xyz_data.info });
            } else {
                wx.navigateBack();
                return false;
            }
        });
    },
    //领取优惠券
    quan_lingqu_bind: function (e) {
        var that = this;
        let quan_id = e.currentTarget.dataset.id || 0;
        _requsetCYZ.cyz_requestGet('/ApiQuanUser/userLingQuan', { quan_id: quan_id }, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.get_data_info();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.quan_lingqu_bind(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //地图
    go_map_bind:function(){
        var that = this;
        wx.openLocation({
            latitude: parseFloat(that.data.this_data_info.shop_list_info.shop_lat),
            longitude: parseFloat(that.data.this_data_info.shop_list_info.shop_lng),
            scale: 18,
            address: that.data.this_data_info.shop_list_info.shop_address
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_data_info();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = '送您一张' + that.data.this_data_info.q_jiner_all_str;
        var sharePath = 'pages/quan/info?quan_id=' + that.data.this_config_op.quan_id + '&tg_user_id=' + that.data.this_login_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    }
})