const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_goods_id: 0,
        this_goods_info:null,
        this_shop_info: null,
        this_rank_list:null,
        this_login_user_id: 0,
        this_goods_photo_index:1,
        this_cut_time:null,
        this_pro_isshow:false,
        this_pro_info:null,
        this_nav_type:1
    },
    setintvalid: null,
    chang_nav_type:function(e){
        this.setData({ this_nav_type: e.currentTarget.dataset.type});
    },
    chang_lunbo_index:function(e){
        this.setData({
            this_goods_photo_index: e.detail.current + 1
        });
    },
    onLoad: function (op) {
        var that = this;
        let goods_id = 0;
        let tg_user_id = 0;
        if (op.id != undefined) {
            goods_id = op.id;
        } else if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            let scene_data = scene.split("#");
            goods_id = scene_data[1] || 0;
            tg_user_id = scene_data[0] || 0;
        }
        that.setData({this_goods_id: goods_id, });
        /*************添加推广************/
        if (op.scene != undefined) {
            let scene = decodeURIComponent(op.scene);
            let scene_data = scene.split("#");
            tg_user_id = scene_data[0] || 0;
        } else if (op.tg_user_id != undefined) {
            if (op.tg_user_id > 0) {
                tg_user_id = op.tg_user_id || 0;
            }
        }
        var requestData = {};
        requestData.top_user_id = tg_user_id || 0;
        _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', requestData, function (cyz_data) {
            if (cyz_data.code == 1) {
                that.setData({ this_login_user_id: cyz_data.info});
                that.get_index_data();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onLoad(op);
                });
            }
        });
    /*************添加推广************/
    },
    get_index_data: function () {
        var that = this;
        app.checkUserGIsName();
        clearTimeout(that.setintvalid);
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        _requsetCYZ.cyz_requestGet('/ApiBargain/getBargainInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_goods_info: cyz_data.info.index_goods_info,
                    this_shop_info: cyz_data.info.index_shop_info,
                    this_rank_list: cyz_data.info.index_rank_list
                });
                //计时器开始
                that.load_time_runing();
                WxParse.wxParse('article', 'html', cyz_data.info.index_goods_info.g_description, that, 5);
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info, "/pages/quan/index", "tab");
                return false;
            }

        });
    },
    //倒计时
    load_time_runing:function(){
        var that = this;
        let goods_data = that.data.this_goods_info;
        if (goods_data.running_all_time <= 0){
            return false;
        }
        var t = goods_data.running_all_time - 1;
        var d = 0;
        var h = 0;
        var m = 0;
        var s = 0;
        var ms = 0;
        if (t >= 0) {
            goods_data.running_all_time = t;
            d = that.checkTime(Math.floor(t / 60 / 60 / 24));
            h = that.checkTime(Math.floor(t / 60 / 60 % 24));
            m = that.checkTime(Math.floor(t / 60 % 60));
            s = that.checkTime(Math.floor(t % 60));
            var cutdown = {};
            cutdown.dd = d;
            cutdown.hh = h;
            cutdown.mm = m;
            cutdown.ss = s;
            that.setData({this_cut_time: cutdown, this_goods_info: goods_data});
        }else{
            clearTimeout(that.setintvalid);
            that.get_index_data();
            return false;
        }
        that.setintvalid = setTimeout(function () {
            that.load_time_runing();
        }, 1000)
    },
    /**
     * 小于10数字的拼接成00类型
     */
    checkTime: function (i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    },
    my_bargain_action:function(e){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        _requsetCYZ.cyz_requestGet('/ApiBargain/myBargainAction', requestData, function (cyz_data) {
            wx.hideToast();
            app.pushFormIdSubmit(e);
            if (cyz_data.code == 1) {
                that.get_index_data();
                that.setData({this_pro_info:cyz_data.info,this_pro_isshow:true});
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.my_bargain_action(e);
                });
                return false;
            } else {
                app.commonErrorTips(cyz_data.info);
                return false;
            }
        });
    },
    hide_pro_show:function(){
        this.setData({ this_pro_isshow:false});
    },
    //我的帮砍
    go_my_help_bind:function(){
        var that = this;
        wx.navigateTo({
            url: '/pages/bargain_help/index?goods_id=' + that.data.this_goods_id + '&user_id=' + that.data.this_login_user_id
        })
    },
    //下单
    gobuy:function(e){
        wx.navigateTo({
            url: '/pages/bargain_submit/index?goods_id='+this.data.this_goods_id,
        })
        app.pushFormIdSubmit(e);
    },
    //图片预览
    preview_look_img: function (e) {
        wx.previewImage({
            current: '',
            urls: [e.currentTarget.dataset.url]
        })
    },
    call_phone_bind:function(e){
        wx.makePhoneCall({
            phoneNumber: e.currentTarget.dataset.mobile,
        });
    },
    //跳转商家
    go_shop_info_action:function(e){
        let _this = this;
        let this_shop_data = _this.data.this_shop_info;
        if(this_shop_data.is_have_wxapp == 1){
            wx.navigateToMiniProgram({
              appId: this_shop_data.have_wxapp_appid
            })
        }else{
            wx.navigateTo({
              url: '/pages/business_detail/index?id=' + this_shop_data.id,
            })
        }
    },
    onUnload: function () {
        clearTimeout(this.setintvalid);
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function (e) {
        console.log(e);
        if (e.type == 'submit') {
            app.pushFormIdSubmit(e);
        }
        var that = this;
        var shareTitle = "帮我砍价↓↓↓【" + that.data.this_goods_info.kan_price + "】元抢购原价" + that.data.this_goods_info.shop_price + "元" + that.data.this_goods_info.g_name;
        var sharePath = 'pages/bargain_help/index?goods_id=' + that.data.this_goods_id + '&user_id=' + that.data.this_login_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})