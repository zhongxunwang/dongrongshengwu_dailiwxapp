const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_goods_id: 0,
        this_user_id:0,
        this_goods_info: null,
        this_pro_info: null,
        this_login_user_id: 0,
        this_cut_time: null,
        this_pro_isshow: false,
        this_helppro_info: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
    },
    setintvalid: null,
    onLoad: function (op) {
        var that = this;
        let goods_id = 0;
        let user_id = 0;
        goods_id  = op.goods_id;
        user_id = op.user_id;
        that.setData({ this_goods_id: goods_id, this_user_id: user_id});
        that.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        clearTimeout(that.setintvalid);
        app.checkUserGIsName();
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        requestData.user_id = that.data.this_user_id;
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ApiBargain/getProgressInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_goods_info: cyz_data.info.index_goods_info,
                    this_pro_info: cyz_data.info.index_pro_info
                });
                if (cyz_data.info.index_pro_info.pro_pro_list == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (cyz_data.info.index_pro_info.pro_pro_list.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                }
                //计时器开始
                that.load_time_runing();
                //添加推广
                var f_requestData = {};
                f_requestData.top_user_id = that.data.this_user_id;;
                _requsetCYZ.cyz_requestGet('/ShopApi/addFenxiaoUser', f_requestData, function (cyz_data) {
                    if (cyz_data.code == 1) {
                        that.setData({ this_login_user_id: cyz_data.info });
                    } else if (cyz_data.code == 2) {
                        app.getUserDataToken(function (token) {
                            that.get_index_data();
                        });
                        return false;
                    }
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info, "/pages/business/index", "tab");
                return false;
            }

        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        requestData.user_id = that.data.this_user_id;
        requestData.pagesize = that.data.this_page_size+1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ApiBargain/getProgressInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info.index_pro_info.pro_pro_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.index_pro_info.pro_pro_list.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
                var this_new_info_data = that.data.this_pro_info;
                this_new_info_data.pro_pro_list = this_new_info_data.pro_pro_list.concat(cyz_data.info.index_pro_info.pro_pro_list);
                that.setData({ this_pro_info: this_new_info_data, this_page_size: requestData.pagesize });
            }
        });
    },
    hide_pro_show: function () {
        this.setData({ this_pro_isshow: false });
    },
    onUnload: function () {
        clearTimeout(this.setintvalid);
    },
    //倒计时
    load_time_runing: function () {
        var that = this;
        let goods_data = that.data.this_goods_info;
        if (goods_data.running_all_time <= 0) {
            return false;
        }
        var t = goods_data.running_all_time - 1;
        var d = 0;
        var h = 0;
        var m = 0;
        var s = 0;
        var ms = 0;
        if (t >= 0) {
            goods_data.running_all_time = t;
            d = that.checkTime(Math.floor(t / 60 / 60 / 24));
            h = that.checkTime(Math.floor(t / 60 / 60 % 24));
            m = that.checkTime(Math.floor(t / 60 % 60));
            s = that.checkTime(Math.floor(t % 60));
            var cutdown = {};
            cutdown.dd = d;
            cutdown.hh = h;
            cutdown.mm = m;
            cutdown.ss = s;
            that.setData({ this_cut_time: cutdown, this_goods_info: goods_data });
        } else {
            clearTimeout(that.setintvalid);
            that.get_index_data();
            return false;
        }
        that.setintvalid = setTimeout(function () {
            that.load_time_runing();
        }, 1000)
    },
    /**
     * 小于10数字的拼接成00类型
     */
    checkTime: function (i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    },
    help_bargain_action:function(e){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = that.data.this_goods_id;
        requestData.user_id = that.data.this_user_id;
        _requsetCYZ.cyz_requestGet('/ApiBargain/helpBargainAction', requestData, function (cyz_data) {
            wx.hideToast();
            app.pushFormIdSubmit(e);
            if (cyz_data.code == 1) {
                that.get_index_data();
                that.setData({ this_helppro_info: cyz_data.info, this_pro_isshow: true });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.help_bargain_action(e);
                });
                return false;
            } else {
                app.commonErrorTips(cyz_data.info);
                return false;
            }
        });
    },
    go_bargain_info:function(e){
        var that = this;
        app.pushFormIdSubmit(e);
        wx.redirectTo({
            url: '/pages/bargain_info/index?id=' + that.data.this_goods_info.id,
        })
    },
    onShareAppMessage: function (e) {
        if (e.type == 'submit') {
            app.pushFormIdSubmit(e);
        }
        var that = this;
        var shareTitle = "帮我砍价↓↓↓【" + that.data.this_goods_info.kan_price + "】元抢购原价" + that.data.this_goods_info.shop_price + "元" + that.data.this_goods_info.g_name;
        var sharePath = 'pages/bargain_help/index?goods_id=' + that.data.this_goods_info.id + '&user_id=' + that.data.this_user_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})